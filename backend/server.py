import urllib.parse
import json
from typing import Optional, List, Dict
from fastapi import FastAPI, Response, Request, Depends, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import os
import requests
import logging

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"])

logger = logging.getLogger("app")

logging.basicConfig(level=logging.INFO)

config = {
        "GITLAB_READ_TOKEN": os.getenv("GITLAB_READ_TOKEN"),
        'GITLAB_WRITE_TOKEN': os.getenv("GITLAB_WRITE_TOKEN"),
        "GITLAB_WRITE_PROJECT_ID": os.getenv("GITLAB_WRITE_PROJECT_ID"),
        "GITLAB_READ_GROUP_ID": os.getenv("GITLAB_READ_GROUP_ID"),
        "DEPLOYMENT_TYPE": os.getenv("DEPLOYMENT_TYPE", "production"),
        }

def read_access(request: Request):
    if config["DEPLOYMENT_TYPE"] == "development":
        return True
    if request.headers.get("private-token") == config["GITLAB_READ_TOKEN"] or request.headers.get("private-token") == config["GITLAB_WRITE_TOKEN"]:
        return True
    else:
        raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Wrong Authorization",
                )

def write_access(request: Request):
    if config["DEPLOYMENT_TYPE"] == "development":
        return True
    if request.headers.get("private-token") == config["GITLAB_WRITE_TOKEN"]:
        return True
    else:
        raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Wrong Authorization",
                )

def log_info(**kwargs):
    return logger.info(str(kwargs))

logger.info(str(config)) 

gitlab_base_url = "https://depot.speald.fr/api/v4" 

def read_headers():
    return {
        "PRIVATE-TOKEN": config["GITLAB_READ_TOKEN"],
        "content-type": "application/json"
        }

def write_headers():
    return {
        "PRIVATE-TOKEN": config["GITLAB_WRITE_TOKEN"],
        "content-type": "application/json"
        }

def list_group_projects_url(group_id=config["GITLAB_READ_GROUP_ID"]):
    return f"{gitlab_base_url}/groups/{group_id}/projects"

def group_details_url(group_id=config["GITLAB_READ_GROUP_ID"]):
    return f"{gitlab_base_url}/groups/{group_id}"

def file_details_url(file_path, ref="master", project_id=config["GITLAB_WRITE_PROJECT_ID"]):
    encoded_file_path = urllib.parse.quote_plus(file_path)
    return f"{gitlab_base_url}/projects/{project_id}/repository/files/{encoded_file_path}?ref={ref}"

def project_details_url(project_id=config["GITLAB_WRITE_PROJECT_ID"]):
    return f"{gitlab_base_url}/projects/{project_id}"

def list_sw_components_url(ref="master", project_id=config["GITLAB_WRITE_PROJECT_ID"]):
    # components are stored in cartographie/components
    return f"{gitlab_base_url}/projects/{project_id}/repository/tree?ref={ref}&path=cartographie%2Fcomponents"

def update_sw_components_url(project_id=config["GITLAB_WRITE_PROJECT_ID"]):
    return f"{gitlab_base_url}/projects/{project_id}/repository/commits"

def to_json(api_response, response: Response):
    response.status_code = api_response.status_code
    try:
        if api_response.headers.get("content-type") == "application/json":
            return api_response.json()
    except:
        return {}

@app.get("/")
def get_config(request: Request):
    private_token = request.headers.get("private-token")
    read = private_token == config["GITLAB_READ_TOKEN"]
    write = private_token == config["GITLAB_WRITE_TOKEN"]
    return {"write": write,
            "read": read or write}

@app.get("/group")
def get_config(response: Response, access: bool = Depends(read_access)):
    """
    Returns the group for GITLAB_READ_GROUP_ID
    """
    api_url = group_details_url()
    log_info(message="details of GITLAB_READ_GROUP_ID", api_url=api_url)
    api_response = requests.get(api_url, headers=read_headers())
    return to_json(api_response, response)

@app.get("/projects")
def get_projects(response: Response, access: bool = Depends(read_access)):
    """
    Lists the projects in the GITLAB_READ_GROUP_ID
    """
    api_url = list_group_projects_url()
    log_info(message="list projects in GITLAB_READ_GROUP_ID", api_url=api_url)
    api_response = requests.get(api_url, headers=read_headers())
    return to_json(api_response, response)

@app.get("/output")
def get_project_output(response: Response, access: bool = Depends(write_access)):
    """
    Returns the project for the GITLAB_WRITE_PROJECT_ID
    """
    api_url = project_details_url()
    log_info(message="details of GITLAB_WRITE_PROJECT_ID", api_url=api_url)
    api_response = requests.get(api_url, headers=write_headers())
    return to_json(api_response, response)

@app.get("/components")
def list_software_components(response: Response, ref: str = "master", access: bool = Depends(write_access)):
    """
    Returns the software components in output (GITLAB_WRITE_PROJECT_ID)
    """
    api_url = list_sw_components_url()
    log_info(message="list software components", api_url=api_url) 
    api_response = requests.get(api_url, headers=write_headers())
    return to_json(api_response, response)

# FIXME: use url encoded value for name
@app.get("/components/{name}")
def list_software_components(name: str, response: Response, ref: str = "master", access: bool = Depends(write_access)):
    """
    Returns the software components in output (GITLAB_WRITE_PROJECT_ID)
    """
    file_path = f"cartographie/components/{name}"
    api_url = file_details_url(file_path)
    params = {'ref': ref}
    log_info(message="details of component", name=name, api_url=api_url, file_path=file_path, params=params) 
    api_response = requests.get(api_url, headers=write_headers(), params={})
    return to_json(api_response, response)

class NewSoftwareComponent(BaseModel):
    name: str
    content: str

@app.post("/components")
def create_software_component(component: NewSoftwareComponent, response: Response, ref: str ="master", access: bool = Depends(write_access)):
    """
    Returns the software components in output (GITLAB_WRITE_PROJECT_ID)
    """
    api_url = update_sw_components_url()
    slug = component.name.replace(" ", "-").lower()
    file_path = f"cartographie/components/{slug}"
    commit = {
            "branch": ref,
            "commit_message": "New sw component",
            "author_email": "bot@speald.com",
            "author_name": "bot-consultant",
            "actions": [
                {
                    "action": "create",
                    "file_path": file_path,
                    "content": component.content,
                    },
                ]
            }
    log_info(api_url=api_url, commit=commit) 
    api_response = requests.post(api_url, headers=write_headers(), data=json.dumps(commit))
    return to_json(api_response, response)

@app.put("/components/{name}")
def update_software_components(name: str, new_content: dict, response: Response, ref: str = "master", last_commit_id: str = "", access: bool = Depends(write_access)):
    """
    Returns the software components in output (GITLAB_WRITE_PROJECT_ID)
    """
    file_path = f"cartographie/components/{name}"
    api_url = update_sw_components_url()
    commit = {
            "branch": ref,
            "commit_message": f"update sw component {name}",
            "author_email": "bot@speald.com",
            "author_name": "bot-consultant",
            "actions": [
                {
                    "action": "update",
                    "file_path": file_path,
                    "content": json.dumps(new_content),
                    "last_commit_id": last_commit_id,
                    },
                ]
            }
    log_info(api_url=api_url, file_path=file_path, commit=commit) 
    api_response = requests.post(api_url, data=json.dumps(commit), headers=write_headers())
    return to_json(api_response, response)

@app.delete("/components/{name}")
def update_software_components(name: str, response: Response, ref: str = "master", last_commit_id: str = "", access: bool = Depends(write_access)):
    """
    Returns the software components in output (GITLAB_WRITE_PROJECT_ID)
    """
    file_path = f"cartographie/components/{name}"
    api_url = update_sw_components_url()
    commit = {
            "branch": ref,
            "commit_message": f"delete sw component {name}",
            "author_email": "bot@speald.com",
            "author_name": "bot-consultant",
            "actions": [
                {
                    "action": "delete",
                    "file_path": file_path,
                    "last_commit_id": last_commit_id,
                    },
                ]
            }
    log_info(api_url=api_url, file_path=file_path, commit=commit) 
    api_response = requests.post(api_url, data=json.dumps(commit), headers=write_headers())
    return to_json(api_response, response)
