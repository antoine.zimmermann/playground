dev-frontend:
	npx shadow-cljs watch frontend

start-playground:
	npx shadow-cljs watch playground

dev-tree-sitter:
	docker run -it -p 9999:9999 -v $(PWD)/src:/app/src tree-sitter yarn shadow-cljs watch tree-sitter
