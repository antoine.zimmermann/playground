FROM python:3.10

WORKDIR /app
COPY backend backend
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

CMD uvicorn backend.server:app --reload --host 0.0.0.0
