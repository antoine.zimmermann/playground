# README

## tools.react

An utility library to work with ReactJS.

### Macros

##### defcomponent

Define a component
```clojurescript
(defcomponent MyComponent
  [props]
  & body)
```

```clojurescript
(<> Component
    :prop1 val1
    :prop2 val2
    & children)
```
