# local development image
FROM ubuntu:20.04 AS build

ENV DEBIAN_FRONTEND=noninteractive
ARG CLOJURE_VERSION='1.10.3.986'

RUN apt-get update -yq

RUN apt-get install -yq curl openjdk-11-jdk 

WORKDIR /tmp

RUN curl -o install_clojure "https://download.clojure.org/install/linux-install-$CLOJURE_VERSION.sh"
RUN chmod +x install_clojure
RUN ./install_clojure

RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -yq nodejs build-essential
RUN npm install -g yarn

WORKDIR /app
COPY ./package.json .
COPY ./public public
COPY ./yarn.lock .
COPY ./shadow-cljs.edn .
COPY ./src src

RUN yarn
RUN yarn shadow-cljs pom tree-sitter

#--------------
