(ns tools.test.react.test
  (:require [tools.react :refer [let-state let-ref let-callback set-state!]]
            [tools.react.test :as t :refer [render-hook current]]
            [cljs.test :refer [deftest testing is]]))


(defn use-render-counter []
  (let-ref [counter 0]
    (swap! counter inc)
    counter))

(defn use-task-provider [context]
  (let-state [tasks* []]
    tasks*))

(defcomponent App
  (render [this context]
          (<> :div
              (<> :h1 "tasks")
              (if (not (:tasks context))
                (<> Message "Loading tasks")
                (<>for [task (:tasks context)]))))
  IHook
  (using [this context]
         (let [tasks* (use-task-provider)]
           (assoc context
                  :loading ()
                  :tasks @tasks*))))

(defn use-selector [items]
  (let-ref [items* (set items)]
    (let-state [selected* (first items)]
      (let-callback [select! (fn [item]
                               (if (contains? @items* item)
                                 (set-state! selected* item)
                                 (println "warning, trying to select an item that was not an option")))]
        {:select! select!
         :options items
         :selected @selected*}))))

(deftest test-hook
  (testing "render-hook"
    (let [expected-value #js {} 
          use-test-hook (fn []
                          expected-value) 
          test-state (render-hook #(use-test-hook))]
      (is (= expected-value (t/current test-state)) "returns expected value")
      (let [values (atom (range 10))
            use-test-hook (fn []
                            (swap! values next))
            test-state (render-hook #(use-test-hook))]
        (is (= 1 (-> test-state t/current first)) "is first coll elem")
        (doseq [v (next @values)]
          (t/rerender! test-state)
          (is (= v (-> test-state t/current first)) "is current coll elem")))
      test-state))
  (testing "counter"
    (let [use-test-counter (fn [initial-count]
                             (let-state [counter initial-count]
                               counter))
          inc! #(set-state! % inc)
          initial-count 0
          test-state (render-hook #(let [counter (use-test-counter initial-count)
                                         render-counter (use-render-counter)]
                                     {:counter counter
                                      :render-counter render-counter}))]
      (let [{:keys [render-counter counter]} (t/current test-state)]
        (is (= initial-count @counter) "first value is the initial state")
        (inc! counter)
        (is (= (inc initial-count) @counter) "value has inc")
        (is (= 2 @render-counter) "has rendered twice")
        test-state)))
  (testing "use-selector"
    (let [items (range 10)
          test-state (render-hook #(let [selector (use-selector items)]
                                     {:selector selector}))
          current-selector #(-> test-state t/current :selector)]
      (is (= (first items) (:selected (current-selector))) "default selected item is the first of the coll")
      (let [select! (:select! (current-selector))]
        (select! (second items))
        (is (= (second items) (:selected (current-selector))) "selected item has updatehas updatedd")
        test-state))))
