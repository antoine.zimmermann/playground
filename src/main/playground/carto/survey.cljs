(ns playground.carto.survey
  (:require [tools.react :as react :refer [defcomponent <> letref ->js]]
            ["survey-react" :as survey]))

(defcomponent Survey
  [props]
  (let [model (:model props)]
    (letref [model* (new survey/Model (->js model))]
      (<> survey/Survey
          :... props
          :data (->js (:data props))
          :model @model*))))
