(ns playground.carto.core
  (:require [tools.react :as react :refer [<> defcomponent]]
            ["@chakra-ui/react" :as ui]
            ["react-query" :as query]
            ["survey-react" :as survey]
            [playground.carto.components :as components]))

(defonce style (do
                 (.applyTheme survey/StylesManager "darkblue")))

(defonce query-client (query/QueryClient.))

(defcomponent App
  [props]
  (<> query/QueryClientProvider
      :client query-client
      (<> ui/ChakraProvider
          (<> components/App))))
