(ns playground.carto.designation
  (:require ["@chakra-ui/react" :as ui]
            [tools.react :as react :refer [defcomponent <> <for> letstate ->clj ->js letcallback]]
            [playground.carto.api :as api]
            ["react-router-dom" :refer [Link]]
            [playground.carto.domain :as d]
            [playground.carto.survey :refer [Survey]]
            [playground.carto.presentation :as presentation]))

(defonce identification* (atom {}))

(defn use-localstorage [save-key]
  (letstate [status* :loading ;; :ready :saving
             data* nil]
    (react/run-effect [status @status*]
                      (case status
                        :loading (js/setTimeout
                                   #(let [saved-data-str (.getItem js/localStorage save-key)]
                                      (react/set-state! data* (->clj (.parse js/JSON saved-data-str)))
                                      (react/set-state! status* :ready))
                                   1500)
                        :saving (let [data @data*]
                                  (js/setTimeout #(do
                                                    (.setItem js/localStorage save-key (.stringify js/JSON (->js data)))
                                                    (react/set-state! status* :ready))
                                                 1500))
                        nil)
                      js/undefined)
    (letcallback [save! (fn [data]
                          (react/set-state! status* :saving)
                          (react/set-state! data* data))]
      {:status @status*
       :data @data*
       :save! save!})))

(defcomponent ListeComposantsLogiciels
  [props]
  (let [{:keys [status data save!]} (use-localstorage "composants-logiciels")]
    (<> ui/Box
        :py 10
        (<> ui/Heading :size "xl"
            "Composants logiciels")
        (case status
          :loading (<> presentation/Loader)
          :saving (<> presentation/Loader)
          :ready (<> Survey 
                     :data data
                     :onComplete #(-> % .-data ->clj save!)
                     :model {:completeText "Sauvegarder"
                             :showCompletedPage false
                             :elements
                             [{:columnColCount 1,
                               :name "software-components"
                               :type "matrixdynamic",
                               :title "Désignez vos composants logiciels",
                               :horizontalScroll true,
                               :rowCount 1,
                               :cellType "radiogroup", 
                               :addRowText "Ajouter un composant",
                               :removeRowText "Supprimer ce composant",
                               :columnMinWidth "130px"    
                               :columns [{:title "Nom du composant"
                                          :name "component-name"
                                          :cellType "text"
                                          :isRequired true
                                          :minWidth "300px"}
                                         {:name "component-technologies",
                                          :cellType "checkbox",
                                          :title "Langages de programmation",
                                          :minWidth "300px",
                                          :hasNone true
                                          :choices
                                          ["Python"
                                           "JavaScript"
                                           "Java"
                                           "C#"
                                           "Elixir / Erlang"
                                           "PHP"
                                           "HTML/CSS"]}
                                         {:name "component-maturity"
                                          :minWidth "300px"
                                          :title "Maturité du composant"
                                          :cellType "radioGroup"
                                          :choices ["En développement"
                                                    "En production depuis 1-6 mois"
                                                    "En production depuis plus de 6 mois"]}
                                         {:name "component-complexity"
                                          :minWidth "300px"
                                          :title "Complexité du composant"
                                          :cellType "radioGroup"
                                          :choices ["Aucun sous composant"
                                                    "1 sous composant"
                                                    "Plus de 2 sous composants"]}
                                         {:name "component-description"
                                          :minWidth "300px"
                                          :title "Description du composant"
                                          :cellType "comment"}]}]})))))

(defcomponent ListeProductionTechnique
  [props]
  (let [{:keys [status data save!]} (use-localstorage "depots-code")
        composants-logiciels (use-localstorage "composants-logiciels")]
    (<> ui/Box
        :py 10
        (<> ui/Heading :size "xl"
            "Dépôts Git")
        (cond
          (and
            (= :ready (:status composants-logiciels))
            (= :ready status)) (<> Survey
                                   :data data
                                   :onComplete #(-> % .-data ->clj save!)
                                   :model {:completeText "Sauvegarder"
                                           :showCompletedPage false
                                           :elements [{:columnColCount 1,
                                                       :name "code-repositories"
                                                       :type "matrixdynamic",
                                                       :title "Désignez les dépôts de code",
                                                       :horizontalScroll true,
                                                       :rowCount 1,
                                                       :addRowText "Ajouter un composant",
                                                       :removeRowText "Supprimer ce composant",
                                                       :columnMinWidth "130px"    
                                                       :columns [{:name "depot-git-nom",
                                                                  :cellType "text",
                                                                  :title "Nom du dépôt Git",
                                                                  :minWidth "300px",}
                                                                 {:name "depot-git-type"
                                                                  :isRequired true
                                                                  :cellType "radiogroup"
                                                                  :choices ["Snapshot" "Fork"]}
                                                                 {:name "depot-git-description"
                                                                  :minWidth "300px"
                                                                  :title "Description longue"
                                                                  :isRequired true
                                                                  :cellType "comment"}
                                                                 {:name "depot-git-composants"
                                                                  :title "Composants présents dans ce dépôt"
                                                                  :minWidth "300px"
                                                                  :isRequired true
                                                                  :cellType "checkbox"
                                                                  :hasNone true
                                                                  :choices (->> composants-logiciels :data :software-components (map :component-name))}]}]})
          :else (<> presentation/Loader)))))

(defcomponent ListeActifsNumériques
  [props]
  (let [{:keys [status data save!]} (use-localstorage "actifs-numeriques")]
    (println "actifs-numeriques" status)
    (<> ui/Box
        :py 10
        (<> ui/Heading :size "xl"
            "Actifs numériques")
        (case status
          :loading (<> presentation/Loader)
          :saving (<> presentation/Loader)
          :ready  (<> Survey
                      :data data
                      :onComplete #(-> % .-data ->clj save!)
                      :model {:completeText "Sauvegarder"
                              :showCompletedPage false
                              :elements [{:columnColCount 1,
                                          :name "actifs-numeriques"
                                          :type "matrixdynamic",
                                          :title "Désignez les actifs numériques",
                                          :horizontalScroll true,
                                          :rowCount 1,
                                          :addRowText "Ajouter un actif numérique",
                                          :removeRowText "Supprimer ce composant",
                                          :columnMinWidth "130px"    
                                          :columns [{:name "actif-description-courte",
                                                     :cellType "text",
                                                     :title "Description courte",
                                                     :minWidth "300px",}
                                                    {:name "actif-description-longue"
                                                     :minWidth "300px"
                                                     :title "Description longue"
                                                     :cellType "comment"}
                                                    {:title "Type d'actif numériques"
                                                     :name "actif-type"
                                                     :cellType "radiogroup"
                                                     :isRequired true
                                                     :choices ["Base de donnée" "Sui generis" "Interface"]
                                                     :minWidth "300px"}
                                                    {:name "actif-droits"
                                                     :minWidth "300px"
                                                     :title "Droits"
                                                     :cellType "checkbox"
                                                     :choices ["Savoir Faire"
                                                               "Secret des affaires"
                                                               "Possession Personnelle Antérieure"
                                                               "Droit d'auteur"
                                                               "Concurrence déloyale"]}
                                                    {:name "actif-contrat"
                                                     :minWidth "300px"
                                                     :title "Utilisation dans des contrats"
                                                     :cellType "checkbox"
                                                     :choices ["Contrat In"
                                                               "Contrat Out"]}
                                                    {:name "actif-fiscalite"
                                                     :minWidth "300px"
                                                     :title "Fiscalité de l'actif"
                                                     :cellType "radiogroup"
                                                     :choices ["Régime de faveur"]
                                                     :hasNone true}]}]})))))
