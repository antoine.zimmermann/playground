(ns playground.carto.api
  (:require [playground.carto.state :as state]
            [playground.carto.domain :as d]
            [tools.react :as react :refer [defcomponent <fragment> <> letstate letcallback letref]]
            [tools.query :as query :refer [use-query]]
            [cljs-bean.core :refer [bean ->clj ->js]]
            ["react-query" :refer [useQuery]]
            ["@chakra-ui/react" :refer [useToast]]))

(def token-context (react/create-context))

(def TokenProvider (.-Provider token-context))

(defn json-response [fetch-response]
  (.then (.json fetch-response)
         (fn [json-body]
           {:body (->clj json-body)
            :status (.-status fetch-response)
            :headers (.-headers fetch-response)})))

(defn fetch [token url opts]
  (-> (js/fetch url
                (->js (assoc-in opts [:headers "PRIVATE-TOKEN"] token)))
      (.then json-response)))

(defcomponent Token
  [{:keys [children]}]
  (letstate [token* (.getItem js/localStorage "token")]
    (let [toast (useToast)
          access-rights* (query/use-mutation (fn [token]
                                               (fetch token (str "http://localhost:8000") {:method "get"}))
                                             {:onSuccess (fn [result]
                                                           (let [body (:body result)] 
                                                             (when (:read body)
                                                               (toast #js {:title "Read access granted"
                                                                           :isClosable true}))
                                                             (when (:write body)
                                                               (toast #js {:title "Write access granted"
                                                                           :isClosable true}))))})]
      (react/run-effect [token @token*]
                        (.setItem js/localStorage "token" token)
                        (query/mutate! access-rights* @token*))
      (react/do-once
        (alter-meta! token* assoc
                     `react/get-status #(react/get-status access-rights*)))
      (<> TokenProvider
          :value token*
          (<fragment> children)))))

(defn use-token
  []
  (react/use-context token-context))

(defn set-token! [token* token]
  (react/set-state! token* token))

(def gitlab-api-endpoint "https://depot.speald.fr/api/v4")

;; get list of user projects
(defn get-projects []
  (-> (js/fetch (str gitlab-api-endpoint "/projects")
             (clj->js {:headers {"PRIVATE-TOKEN" (state/get-token)}}))
      (.then (fn [res]
               (if (= 200 (.-status res))
                 (-> res .json (.then ->clj))
                 (throw (ex-info "Invalid response" {:response res})))))))

(defn use-projects []
  (let [token* (state/use-token)]
    (use-query #js [@token* "projects"]
               #(get-projects)
               #js {:enabled (not (nil? @token*))})))

;; get trees for selected project
(defn get-tree
  ([project-id ref-name]
   (get-tree project-id ref-name nil))
  ([project-id ref-name path]
   (-> (js/fetch (str gitlab-api-endpoint "/projects/"project-id"/repository/tree?ref="ref-name (when path (str "&path="path)))
                 (clj->js {:headers {"PRIVATE-TOKEN" (state/get-token)}}))
       (.then (fn [res]
                (if (= 200 (.-status res))
                  (-> res .json (.then ->clj))
                  (throw (ex-info "Invalid response" {:response res}))))))))

(defn use-project-tree
  ([]
   (use-project-tree nil))
  ([path]
   (let [token* (state/use-token)
         project-id* (state/use-selected-project)
         branch* (state/use-selected-branch)]
     (use-query #js [@token* "projects" @project-id* "tree" @branch* path]
                #(get-tree @project-id* @branch* path)
                #js {:enabled (and (not (nil? @token*))
                                   (not (nil? @project-id*))
                                   (not (nil? @branch*)))}))))
;; get branches for selected project
(defn get-branches [project-id]
  (-> (js/fetch (str gitlab-api-endpoint "/projects/" project-id "/repository/branches")
             (clj->js {:headers {"PRIVATE-TOKEN" (state/get-token)}}))
      (.then (fn [res]
               (if (= 200 (.-status res))
                 (-> res .json (.then ->clj))
                 (throw (ex-info "Invalid response" {:response res})))))))

(defn use-project-branches []
  (let [token* (state/use-token)
        project-id* (state/use-selected-project)]
    (use-query #js [@token* "projects" @project-id* "branches"]
               #(get-branches @project-id*)
               #js {:enabled (and (not (nil? @token*))
                                  (not (nil? @project-id*)))})))

(defn get-file [token project-id ref-name file-path]
  (fetch token
         (str gitlab-api-endpoint "/projects/" project-id "/repository/files/"(when file-path (js/encodeURIComponent file-path))"?" (str "ref="ref-name))
         {}))

(defn use-project-file [project-id branch file-path]
  (let [token* (use-token)
        enabled (and @token*
                     project-id
                     (and (string? file-path)
                          (not= "" file-path)))]
    (use-query #js ["projects" project-id "file" file-path]
               #(get-file @token* project-id branch file-path)
               #js {:enabled enabled
                    :retry 0})))

(defn get-component-query-key
  [component-name]
  #js ["component" component-name])

(defn update-component-file-data
  [components component-file-data]
  (map
    (fn [component]
      (if (= (d/get-name component) (:file_name component-file-data))
        (react/assoc-meta!
          (merge component component-file-data)
          `react/get-status (constantly :ready))
        component))
    components))

(defn fetch-components
  [token]
  (-> (fetch token
             (str "http://localhost:8000/components")
             {:method "GET"})
      (.then #(:body %))))

(defn fetch-component-detail
  [token component-name]
  (-> (fetch token
             (str "http://localhost:8000/components/" component-name)
             {:method "GET"})
      (.then #(:body %))))

(defn get-component-status
  [component]
  :ready)

(defn- get-component-status-impl [component]
  (let [query (-> component meta (:query {}))
                             {:keys [isFetched isFetching]} query]
                         (cond
                           (and isFetched (not isFetching))
                           :ready
                           (and isFetching isFetched)
                           :reloading
                           :default
                           :loading)))

(def component-protocol-impls
  {`react/get-status get-component-status-impl
   `d/get-name :file_name
   `d/get-last-commit-id :last_commit_id})

(defn ->component [o & ms]
  (apply vary-meta o merge component-protocol-impls ms))

(defn- get-components-status
  "Local implementation of react/get-status"
  [components-list*]
  (let [components (seq @components-list*)
        statuses (map react/get-status components)]
    (cond
      (every? #{:ready} statuses)
      :ready
      (some #{:reloading} statuses)
      :reloading
      :default
      :loading)))

(defn- use-list-components-names
  "Internal hook. Returns a query resolving to the list of components"
  []
  (let [token* (use-token)]
    (query/use-query
      #js ["sw-components"]
      #(fetch-components @token*)
      {:select (fn [components]
                 (map :name components))})))

(defn- use-list-component-files
  "Internal hook. Returns an array of queries resolving to the files of the component names."
  [component-names]
  (let [token* (use-token)]
    (map :val
         (query/use-queries*
           (->js (map (fn [component-name]
                        {:queryKey (get-component-query-key component-name)
                         :initialData (constantly {:file_name component-name})
                         :queryFn #(fetch-component-detail @token* component-name)})
                      component-names))))))

(defn use-list-sw-components
  []
  (let [token* (use-token)]
    (letref [components* ()]
      (let [component-names (use-list-components-names)
            sw-components-files-queries (use-list-component-files (:data component-names))]
        (react/do-once
          (react/assoc-meta! components*
                             `react/get-status get-components-status))
        (reset! components*
                (map (fn [component-query]
                       (->component (:data component-query)
                                    {:query (dissoc component-query :data)}))
                     sw-components-files-queries))
        components*))))

(defn ->json-str [o]
  (.stringify js/JSON (->js o)))

(defn use-create-sw-component
  [on-success]
  (let [token* (use-token)
        create-sw-component* (query/use-mutation
                               (fn [component]
                                 (-> (fetch @token*
                                            (str "http://localhost:8000/components")
                                            {:method "POST"
                                             :headers {:content-type "application/json"}
                                             :body (->json-str component)})
                                     (.then #(:body %))))
                               {:onSuccess (fn [commit component]
                                             (on-success component commit))})]
    (fn [component]
      ((:mutate create-sw-component*) component))))

(defn- delete-component-impl [component]
  (let [delete-mutation (-> component meta ::delete-mutation)]
    (assert delete-mutation "Did you call use-component-with-delete")
    (println delete-mutation)
    ((:mutate delete-mutation))))

(defn- get-deletable-component-status-impl
  [component]
  (let [delete-mutation (-> component meta ::delete-mutation)]
    (assert delete-mutation "Did you call use-component-with-delete")
    (cond (:isSuccess delete-mutation)
          :deleted
          (:isLoading delete-mutation)
          :deleting
          :default
          (get-component-status-impl component))))

(defn- ->with-delete [component delete-component-mutation]
  (react/assoc-meta! component
                     ::delete-mutation delete-component-mutation 
                     `d/delete! delete-component-impl
                     `react/get-status get-deletable-component-status-impl))

(defn delete-component [token component-name last-commit-id]
  (assert (and component-name last-commit-id) "component-name and last-commit-id required")
  (-> (fetch token
             (str "http://localhost:8000/components/" component-name "?last_commit_id="last-commit-id"&")
             {:method "DELETE"
              :headers {:content-type "application/json"}})
      (.then :body)))

(defn use-component-with-delete
  [component]
  (let [token* (use-token)
        query-client (query/use-query-client)
        on-delete-success (fn []
                            (.invalidateQueries query-client
                                                (get-component-query-key (d/get-name component)))
                            (.invalidateQueries query-client
                                                #js ["sw-components"]))
        delete-component-mutation (query/use-mutation
                                    #(delete-component @token*
                                                       (d/get-name component)
                                                       (d/get-last-commit-id component))
                                    {:onSuccess on-delete-success})]
    (->with-delete component delete-component-mutation)))
