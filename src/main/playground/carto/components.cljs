(ns playground.carto.components
  (:require [tools.react :as react :refer [<>
                                           <for>
                                           <provide>
                                           <fragment>
                                           defcomponent
                                           letref
                                           letstate
                                           run-effect
                                           letcallback]]
            [tools.callbag :as callbag]
            [cljs-bean.core :refer [bean ->clj ->js]]
            ["@chakra-ui/react" :as ui]
            ["@chakra-ui/icons" :as icons]
            [playground.carto.state :as state]
            [playground.carto.editor :as ed]
            [playground.carto.actif :as actif-num]
            [playground.carto.questions :as questions]
            [playground.carto.api :as api :refer [Token use-token set-token!]]
            [playground.carto.survey :as survey]
            [playground.carto.domain :as d]
            [playground.carto.presentation :as presentation]
            [playground.carto.designation :as designation]
            [tools.react.utils :as rutils]
            ["js-yaml" :as yaml]
            ["react-router-dom" :refer [Link BrowserRouter Routes Route Navigate useNavigate]]))



(defcomponent TokenInput
  [props]
  (let [token* (use-token)]
    (letstate [input* (or @token* "")]
      (let [navigate (useNavigate)
            token-status (if (= @token* @input*) :saved :dirty)
            button-colorscheme (if (= :saved token-status) "green" "red")
            button-message (if (= :saved token-status) "Token saved" "Save token")]
        (<> ui/Box
            :as "form"
            :onSubmit (fn [submit-event]
                        (set-token! token* @input*)
                        (.preventDefault submit-event)
                        (navigate "/"))
            (<> ui/InputGroup
                :size "md"
                :width "30rem"
                (<> ui/Input
                    :name "token"
                    :placeholder "GITLAB_API_TOKEN"
                    :onChange (fn [e]
                                (react/set-state! input* (-> e .-target .-value)))
                    :value (or @input* ""))
                (<> ui/InputRightElement
                    :width "7rem"
                    (<> ui/Button
                        :type "submit"
                        :size "sm"
                        :display "inline-block"
                        :colorScheme button-colorscheme
                        button-message))))))))

(declare TreeLabel)

(declare Tree)

(defcomponent TreeLabel
  [{:keys [tree-item]}]
  (letstate [is-open* false]
    (letcallback [on-toggle-subtree (fn []
                                      (react/update-state! is-open* not))]
      (let [ChevronIcon (if @is-open*
                          icons/ChevronDownIcon
                          icons/ChevronRightIcon)]
        (<> react/Fragment
            (<> ui/Box
                (<> ChevronIcon
                    :onClick on-toggle-subtree
                    :cursor "pointer")
                (:name tree-item)
                (when @is-open*
                  (<> Tree
                      :path (:path tree-item)))))))))

(defcomponent Blob
  [{:keys [tree-item]}]
  (when (not= "blob" (:type tree-item))
    (throw (ex-info "Invalid tree item, expecting a blob" {:tree-item tree-item})))
  (let [blob tree-item
        selected-blob* (state/use-selected-blob)
        selected? (= (:id blob) @selected-blob*)]
    (letcallback [on-file-select (fn [event]
                                   (if selected?
                                     (state/select-blob! nil)
                                     (state/select-blob! (:id blob))))]
      (<> react/Fragment
          (<> icons/EditIcon
              :cursor "pointer"
              :onClick on-file-select)
          (:name blob)
          (when selected?
            (<> ui/UnorderedList
                :spacing 3
                (<> ui/ListItem
                    (<> ui/Button
                        :colorScheme "blue"
                        :onClick (fn []
                                   (state/set-editor-blob! blob))
                        "See in editor"))
                (<> ui/ListItem (<> ui/Button :colorScheme "green" "See details"))))))))

(defcomponent Tree
  [props]
  (let [project-tree-query (api/use-project-tree (:path props))]
    (if (:isSuccess project-tree-query)
      (<> ui/List
          :marginLeft "2rem"
          (<for> [tree-item (:data project-tree-query)]
                 (<> ui/ListItem
                     :key (:id tree-item)
                     (case (:type tree-item)
                       "tree" (<> TreeLabel :tree-item tree-item)
                       "blob" (<> Blob :tree-item tree-item)
                       (do (println "this guy is neither a tree nor a blob" tree-item)))))))))

(defcomponent Branches
  [props]
  (let [selected-branch* (state/use-selected-branch)
        branches (api/use-project-branches)]
    (when (:data branches)
      (<> ui/UnorderedList
          (<for> [branch (:data branches)
                  :let [branch-name (:name branch)]]
                 (<> ui/ListItem
                     :key branch-name
                     :cursor "pointer"
                     :onClick (fn [e]
                                (state/select-branch! branch-name))
                     (<> ui/Text
                         :textDecoration (if (= branch-name @selected-branch*) "underline" "none")
                         branch-name)))))))

(defcomponent ProjectsList
  [props]
  (let [projects-query (api/use-projects)
        selected-project* (state/use-selected-project)
        token* (state/use-token)]
    (if (nil? @token*)
      "Please enter a valid token"
      (if-let [projects (:data projects-query)]
        (<> ui/UnorderedList
            (<for> [project projects]
                   (<> ui/ListItem
                       :key (:id project)
                       (<> ui/Box
                           (<> ui/Heading 
                               :cursor "pointer"
                               :size "lg"
                               :as "span"
                               :textDecoration (if (= @selected-project* (:id project)) "underline" "none")
                               :onClick (fn [e]
                                          (state/select-project! (:id project)))
                               (:name project)
                               (<> ui/Link
                                   :target "_blank"
                                   :marginLeft "1rem"
                                   :title "web url"
                                   :href (:web_url project)
                                   (<> icons/ExternalLinkIcon)))))))
        "Loading..."))))

(def Space ui/Box)

(defcomponent EnsureToken
  [{:keys [children]}]
  (let [token* (use-token)]
    (if-not @token*
      (<> Navigate :to "/login")
      (<fragment> children))))

(defcomponent Login
  [props]
  (<> ui/Container
      :h "100%"
      (<> ui/Flex
          :h "100%"
          :direction "column"
          :align "center"
          :justify "center"
          (<> ui/Heading "Entrez votre mot de passe")
          (<> TokenInput))))

(defcomponent Designer
  [props]
  (<> ui/Box
      (<> designation/ListeComposantsLogiciels)
      (<> designation/ListeProductionTechnique)
      (<> designation/ListeActifsNumériques)))

(defcomponent PageNouveauComposantLogiciel
  [props]
  (let [page-state* (letstate [state* {:status :editing
                                       :components-to-create ()
                                       :components-created ()}]
                      (react/assoc-meta! state*
                                         `react/get-status #(-> % deref :status)))
        on-component-created (fn [commit component]
                               (react/update-state! page-state*
                                 (fn [{:keys [components-to-create components-created] :as state}]
                                   (let [rest-to-create (next components-to-create)]
                                     (assoc state
                                            :status (if rest-to-create :creating :done)
                                            :components-to-create (or rest-to-create ())
                                            :components-created (conj components-created (first components-to-create)))))))
        create-new-component (api/use-create-sw-component on-component-created)
        on-create-new-composant (fn [survey]
                                  (let [result (->clj (.-data survey))
                                        components (map
                                                     (fn [component-data]
                                                       {:name (:component-name component-data)
                                                        :content (yaml/dump (->js component-data))})
                                                     (:software-components result))]
                                    (react/update-state! page-state* assoc
                                                         :status :creating
                                                         :components-to-create components)))
        toast (ui/useToast)]
    (react/run-effect
      [components-to-create-count (count (:components-to-create @page-state*))]
      (when (pos? components-to-create-count)
        (let [new-component (first (:components-to-create @page-state*))]
          (create-new-component new-component))))
    (<> ui/Container
        :maxW "container.xl"
        (<> ui/Heading "Nouveau composant logiciel")
        (<> ui/Box
            :mt 10 
            (cond
              (= :editing (react/get-status page-state*))
              (<> survey/Survey
                  :model questions/composants-logiciels
                  :onComplete on-create-new-composant)
             (= :creating (react/get-status page-state*)) 
             (<> presentation/Loader)
             (= :done (react/get-status page-state*))
             (<> Navigate :to "/designer"))))))

(defcomponent PageEditionComposantLogiciel
  [props]
  (<> ui/Box
      (<> ui/Heading "Edition du composant logiciel")
      (<> survey/Survey :model questions/edition-composant-logiciel)))

(def bg-colors {:lighter "gray.100"
                :gray "gray.600"
                :light-dark "gray.700"
                :dark "gray.800"
                :darker "gray.900"})

(def nav-items [{:label "Désignation"
                 :content "Désigner vos composants logiciels et vos actifs numériques"
                 :url "/designer"}
                {:label "Protection"
                 :content "Protégez vos actifs"
                 :url "/proteger"}
                {:label "Valorisation"
                 :content "Valorisz vos actifs"
                 :url "/valoriser"}])

(defcomponent DesktopNav
  [props]
  (<> ui/Stack :direction "row" :spacing 4
      (<for> [item nav-items
              :let [{:keys [label content url]} item]]
             (<> ui/Box
                 :key label
                 (<> ui/Popover :trigger "hover" :placement "bottom-start"
                     (<> ui/PopoverTrigger
                         (<> ui/Link
                             :as Link
                             :to url
                             :p 2
                             :href "#" 
                             :fontSize "sm"
                             :fontWeight 500
                             :_hover #js {:textDecoration "none"
                                          :color "white"}
                             label))
                     (when content
                       (<> ui/PopoverContent
                          :border 0
                          :boxShadow "xl"
                          :bg "gray.800"
                          :minW "sm"
                          :rounded "xl"
                          :textAlign "center"
                          :py 3
                          content)))))))

(defcomponent Navbar
  [{:keys [children]}]
  (let [disclosure (ui/useDisclosure)]
    (letcallback [on-toggle #(rutils/on-toggle disclosure)]
      (<> ui/Box
          (<> ui/Flex
              :bg "blue.900"
              :color "gray.100"
              :minHeight "60px"
              :px #js {:base 2}
              :py #js {:base 4}
              :borderBottom 1
              :borderStyle "solid"
              :borderColor (:darker bg-colors)
              :align "center"
              (<> ui/Flex
                  :flex #js {:base 1 :md "auto"}
                  (<> ui/IconButton
                      :colorScheme "blue"
                      :variant "ghost"
                      :aria-label "Toggle Navigation"
                      :onClick on-toggle
                      :icon (if (rutils/is-open disclosure)
                              (<> icons/CloseIcon :w 3 :h 3)
                              (<> icons/HamburgerIcon :w 3 :h 3))))
              (<> ui/Flex
                  :flex #js {:base 1}
                  :justify #js {:base "center" :md "start"}
                  (<> ui/Link
                      :as Link
                      :to "/"
                      :textAlign #js{:base "center" :md "start"}
                      :fontFamily "heading"
                      :color "white"
                      "SPEALD"))
              (<> ui/Flex
                  :display #js {:base "none" :md "flex"} :ml 10
                  (<> DesktopNav)))))))

(defcomponent App
  [props]
  (<> BrowserRouter
      (<> Token
          (<> ui/Flex
              :color "gray.700"
              :bg "gray.100"
              :minH "100vh"
              :direction "column"
              (<> Navbar)
              (<> ui/Container
                  :paddingTop "2rem"
                  :maxW "container.xl"
                  :position "relative"
                  :flex "auto"
                  (<> Routes
                      (<> Route :path "/"
                          :element (<> EnsureToken
                                       (<> ui/Container
                                           (<> ui/Heading
                                               "Bienvenue dans l'assistant de cartographie numérique !"))))
                      (<> Route :path "/designer"
                          :element (<> Designer))
                      (<> Route :path "/designer/nouveau-composant"
                          :element (<> PageNouveauComposantLogiciel))
                      (<> Route :path "/designer/:componentName/edit"
                          :element (<> PageEditionComposantLogiciel))
                      (<> Route :path "/proteger"
                          :element (<> ui/Box "proteger"))
                      (<> Route :path "/valoriser"
                          :element (<> ui/Box "valoriser"))
                      (<> Route :path "/login"
                          :element (<> Login))))))))
