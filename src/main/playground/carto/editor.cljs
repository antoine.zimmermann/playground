(ns playground.carto.editor
  (:require [tools.react :as react :refer [<> letref letcallback ReactRef create-element]]
            ["@monaco-editor/react" :as monaco]
            [tools.callbag :as callbag]
            [cljs-bean.core :refer [->js]]))

 
(def MonacoEditor (.-default monaco))

(defprotocol ISelection
  :extend-via-metadata true
  (get-text-range [this]))

(defprotocol IEditorObject
  :extend-via-metadata true
  (get-editor [this]))

(defprotocol IEditor
  :extend-via-metadata true
  (get-selection [this])
  (get-model [this]))

(defn mounted?
  [editor]
  (seq @editor))

(defn get-editor-model
  [editor]
  (.getModel ^js (:editor (meta editor))))

(defn get-selection-text-range
  [selection]
  (let [{:keys [editor raw-selection]} (meta selection)
        model (get-model editor)]
    ^js (.getValueInRange model raw-selection)))

(defn get-editor-selection
  [editor]
  (let [ed (:monaco/editor @editor)]
    (when ed
      (when-let [selection (.getSelection ed)]
        ^{:raw-selection selection
          `get-editor (constantly editor)
          `get-text-range get-selection-text-range}
        {:start-column ^js/Number (.-startColumn selection)
         :end-column ^js/Number (.-endColumn selection)
         :end-line-number ^js/Number (.-endLineNumber selection)
         :start-line-number ^js/Number (.-startLineNumber selection)}))))

(defn use-monaco-editor [{:keys [actions value]}]
  (letref [editor* {}]
    (letcallback [editor-did-mount (fn [^js editor ^js monaco]
                                         (reset! editor* {:monaco/editor editor
                                                          :monaco monaco})
                                         ; https://microsoft.github.io/monaco-editor/playground.html#interacting-with-the-editor-adding-an-action-to-an-editor-instance
                                         (doseq [action actions]
                                           (.addAction editor (->js action))))
                  create-editor-element (fn [_ props _]
                                          (<> MonacoEditor
                                              :... props
                                              :onMount editor-did-mount
                                              :defaultLanguage "yaml"
                                              :height "90vh"))]
      (alter-meta! editor* assoc
                   `create-element create-editor-element
                   `get-selection get-editor-selection)
      editor*)))
