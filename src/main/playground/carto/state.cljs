(ns playground.carto.state
  (:require [tools.react :as react :refer [letstate run-effect]]))


(def default-state {:tab/main 0
                    :tab/list {0 :tab/config
                               1 :tab/explorer
                               2 :tab/editor}
                    :tab/config 0
                    :snippets/list []})

(defonce state* (atom default-state))

(defn reset-state! []
  (reset! state* default-state))

(defn use-global-state [select-fn]
  (letstate [out* (select-fn @state*)]
    (run-effect :once
                (let [on-state-change (fn [k s o n]
                                        (let [new-out (select-fn n)]
                                          (if (not= new-out @out*)
                                            (react/set-state! out* new-out))))
                      k on-state-change]
                  (add-watch state* k on-state-change)
                  (fn []
                    (remove-watch state* k))))
    out*))


(defn set-token! [token]
  (swap! state* assoc :config/token token))

(defn get-token
  ([] (get-token @state*))
  ([state] (:config/token state)))

(defn new-token? [old-state new-state]
  (not= (get-token old-state) (get-token new-state)))

(defn use-token
  []
  (use-global-state get-token))

(defn select-project! [project-id]
  (swap! state* assoc :config/project project-id))

(defn get-project
  ([] (get-project @state*))
  ([state] (:config/project state)))

(defn use-selected-project
  []
  (use-global-state get-project))

(defn select-branch!
  [branch]
  (swap! state* assoc :config/branch branch))

(defn get-branch
  ([] (get-branch @state*))
  ([state] (:config/branch state)))

(defn use-selected-branch
  []
  (use-global-state get-branch))

(defn select-blob!
  [blob-id]
  (swap! state* assoc :config/blob blob-id))

(defn get-selected-blob
  ([] (get-selected-blob @state*))
  ([state] (:config/blob state)))

(defn use-selected-blob
  []
  (use-global-state get-selected-blob))

(defn set-editor-blob!
  [blob-id]
  (swap! state* assoc :editor/blob blob-id))

(defn get-editor-blob
  ([] (get-editor-blob @state*))
  ([state] (:editor/blob state)))

(defn use-editor-blob
  []
  (use-global-state get-editor-blob))

(defn get-main-tab
  ([] (get-main-tab @state*))
  ([state] (:tab/main state)))

(defn set-main-tab! [tab-index]
  (swap! state* assoc :tab/main tab-index))

(defn use-main-tab []
  (use-global-state get-main-tab))

(defn get-config-tab
  ([] (get-config-tab @state*))
  ([state] (:tab/config state)))

(defn set-config-tab! [tab-index]
  (swap! state* assoc :tab/config tab-index))

(defn use-config-tab []
  (use-global-state get-config-tab))

(defn get-editor-file
  ([] (get-editor-file @state*))
  ([state] (:editor/file state)))

(defn set-editor-file! [file]
  (swap! state* assoc :editor/file file))

(defn use-editor-file []
  (use-global-state get-editor-file))

(defn get-editor-file-details
  ([] (get-editor-file-details @state*))
  ([state] (dissoc (get-editor-file state) :raw :content)))

(defn use-editor-file-details
  []
  (use-global-state get-editor-file-details))

(defn get-snippets
  [state]
  (:snippets/list state []))

(defn use-snippets []
  (use-global-state get-snippets))

(defn add-snippet!
  [snippet]
  (swap! state* update :snippets/list conj snippet))

(comment
  (:snippets/list @state*))
