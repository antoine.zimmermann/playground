(ns playground.carto.presentation
  (:require [tools.react :as react :refer [defcomponent
                                           <for>
                                           <>]]
            [playground.carto.domain :as d]
            ["react-router-dom" :refer [Link]]
            ["@chakra-ui/react" :as ui]))

(defcomponent Loader
  [props]
  (<> ui/Flex
      :position "absolute"
      :top 0
      :bottom 0
      :left 0
      :right 0
      :justify "center"
      :align "center"
      (<> ui/CircularProgress :isIndeterminate true :color "blue.800")))

(defcomponent ComposantLogicielListItem
  [{:keys [component edit-url] :as props}]
  (let [component-name (d/get-name component)
        status (react/get-status component)]
    (<> ui/ListItem
        :key component-name
        ;; Card Composant logicel
        (<> ui/Box
            :p 3
            :rounded 4
            :color "gray.200"
            :bg "blue.800"
            (<> ui/Heading 
                :size "md"
                :textDecoration "underline"
                component-name)
            ; "bla bla bla description"
            (<> ui/Flex
                :pt 5 
                :justify "space-between"
                (<> ui/Button 
                    :as Link
                    :to edit-url
                    :colorScheme "white"
                    :variant "outline"
                    "Éditer")
                (<> ui/Button 
                    :isLoading (= :deleting status)
                    :colorScheme "red"
                    :onClick (fn []
                               (d/delete! component))
                    "Supprimer"))))))

;; on-component-delete: callback, receives the component to be deleted
;; component: a collection of IComposant
(defcomponent ListeComposantsLogiciels
  [{:keys [components on-component-delete children]}]
  (<> ui/List
      :spacing 10 
      :mt 10 
      children))

(defcomponent EditionComposantLogiciel
  [props]
  (<> ui/Box
      "hello world"))
