(ns playground.carto.domain
  (:require [tools.react :as react]))

(defprotocol IComposant
  :extend-via-metadata true
  (get-name [this] "Un composant logiciel peut se désigner par son nom qui est unique et fourni par le client.")
  (delete! [this] "Supprime le composant")
  (get-content-str [this])
  (get-last-commit-id [this])
  (get-file-path [this] "Un composant logiciel est stocké comme un fichier sur GITLAB. Retourne son chemin depuis la racine du dépôt"))
