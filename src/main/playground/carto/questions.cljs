(ns playground.carto.questions
  (:require [tools.react :as react :refer [defcomponent <> letref]]
            [cljs-bean.core :refer [->js]]
            ["survey-react" :as survey]))

(def composants-logiciels 
  {:completeText "Sauvegarder"
   :showCompletedPage false
   :elements
   [{:columnColCount 1,
     :name "software-components"
     :type "matrixdynamic",
     :title "Désignez vos composants logiciels",
     :horizontalScroll true,
     :rowCount 1,
     :cellType "radiogroup", 
     :addRowText "Ajouter un composant",
     :removeRowText "Supprimer ce composant",
     :columnMinWidth "130px"    
     :columns [{:title "Nom du composant"
                :name "component-name"
                :cellType "text"
                :isRequired true
                :minWidth "300px"}
               {:name "component-technologies",
                :cellType "checkbox",
                :title "Environnements d'éxécutions",
                :minWidth "300px",
                :hasNone true
                :choices
                ["Python"
                 "Nodejs"
                 "JVM (Java)"
                 ".Net (C#)"
                 "Elixir / Erlang"
                 "PHP"
                 "HTML/CSS"]}
               {:name "component-maturity"
                :minWidth "300px"
                :title "Maturité du composant"
                :cellType "radioGroup"
                :choices ["En développement"
                          "En production depuis 1-6 mois"
                          "En production depuis plus de 6 mois"]}
               {:name "component-complexity"
                :minWidth "300px"
                :title "Complexité du composant"
                :cellType "radioGroup"
                :choices ["Aucun sous composant"
                          "1 sous composant"
                          "Plus de 2 sous composants"]}
               {:name "component-description"
                :minWidth "300px"
                :title "Description du composant"
                :cellType "comment"}]}]})

(def edition-composant-logiciel
  {:completeText "Sauvegarder"
   :showCompletedPage false
   :elements [{:title "Nom du composant"
               :name "component-name"
               :type "text"
               :isRequired true
               :minWidth "300px"}
              {:name "component-technologies",
               :type "checkbox",
               :colCount 3
               :title "Environnements d'éxécutions",
               :minWidth "300px",
               :hasNone true
               :choices
               ["Python"
                "Nodejs"
                "JVM (Java)"
                ".Net (C#)"
                "Elixir / Erlang"
                "PHP"
                "HTML/CSS"]}
              {:name "component-maturity"
               :minWidth "300px"
               :title "Maturité du composant"
               :type "radioGroup"
               :hasNone true
               :noneText "Non connu"
               :choices ["En développement"
                         "En production depuis 1-6 mois"
                         "En production depuis plus de 6 mois"]}
              {:name "component-complexity"
               :minWidth "300px"
               :title "Complexité du composant"
               :type "radioGroup"
               :noneText "Non connu"
               :hasNone true
               :choices ["Aucun sous composant"
                         "1 sous composant"
                         "Plus de 2 sous composants"]}
              {:name "component-description"
               :minWidth "300px"
               :title "Description du composant"
               :type "comment"}]})
