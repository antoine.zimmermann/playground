(ns playground.carto.actif
  (:require [tools.query :as query]
            [cljs-bean.core :refer [bean]]
            ["react-query" :as react-query]
            [playground.elasticsearch.core :as es]))

(defprotocol IActifNumerique
  :extend-via-metadata true
  (get-id [actif])
  (get-nom [actif]))

(extend-protocol IActifNumerique
  default
  (get-id [actif] "PLACEHOLDER")
  (get-nom [actif] "PLACEHOLDER"))

(def actifs-numeriques-query-key #js ["actifs-numeriques"])

(defn use-liste-actifs-numeriques []
  (let [query (query/use-query actifs-numeriques-query-key
                               (fn []
                                 ()))]
    (alter-meta! query assoc
                 `query/data
                 (fn [q]
                   (-> q :data :hits :hits)))
    query))


(defn use-create-actif-numerique
  [on-create-actif]
  (let [query-client (react-query/useQueryClient)
        mutation (query/use-mutation (fn [new-actif]
                                      (es/post-doc! es/index-carto-numerique new-actif))
                                    #js {:onSuccess (fn [response]
                                                      (js/setTimeout
                                                        #(.refetchQueries query-client actifs-numeriques-query-key)
                                                        1000)
                                                      (on-create-actif response))})]
    mutation))

(defn use-delete-actif-numerique
  [on-delete-actif]
  (let [query-client (react-query/useQueryClient)
        mutation (query/use-mutation (fn [actif]
                                      (es/delete-doc! es/index-carto-numerique (get-id actif)))
                                    #js {:onSuccess (fn [response]
                                                      (js/setTimeout
                                                        #(.refetchQueries query-client actifs-numeriques-query-key)
                                                        1000)
                                                      (on-delete-actif response))})]
    mutation))
