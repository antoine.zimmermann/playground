(ns playground.akfox.core
  (:require [tools.react :as react :refer [defcomponent <>]]
            ["@chakra-ui/react" :as ui]))

(defcomponent Form
  [props]
  (<> ui/Box "Form"))

(defcomponent App
  [props]
  (<> ui/ChakraProvider
      (<> ui/Container
          :maxW "container.xl"
          (<> Form))))
