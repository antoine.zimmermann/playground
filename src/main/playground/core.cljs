(ns playground.core
  (:require [cljs-bean.core :refer [->clj]]
            [tools.react.callbag :refer []]
            [tools.callbag :as callbag :refer [as-source]]
            [tools.react :as react :refer [defcomponent
                                           <>
                                           <>for
                                           letstate
                                           letcallback
                                           letref
                                           create-context
                                           set-state!
                                           update-state!
                                           use-context]]
            [tools.react-dom :refer [render! create-root]]
            [playground.elasticsearch.core :as es]
            [playground.carto.core :as carto]
            [playground.akfox.core :as akfox]
            [playground.youtube.core :as yt]))

(defn start! [])

(def root (create-root "#main-container"))


; https://microsoft.github.io/monaco-editor/playground.html#interacting-with-the-editor-adding-an-action-to-an-editor-instance
; demo adding a label in the contextual menu

; (render! root
;          (<> carto/App))


(def app carto/App)

(render! root
         (<> app))

(defn ^:dev/after-load on-reload []
  (render! root (<> app)))
