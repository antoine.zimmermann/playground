(ns playground.elasticsearch.core
  (:require [tools.react :as react :refer [defcomponent <> <>for]]
            ["react-query" :as query]
            [cljs-bean.core :refer [bean ->clj ->js]]))

(defn write-json [o]
  (.stringify js/JSON o))

(defn parse-json [s]
  (.parse js/JSON s))

(def es-endpoint "http://localhost:9200")

(defn post-doc!
  [index-name doc]
  (-> (js/fetch
        (str es-endpoint"/"index-name"/_doc?")
        (->js {:method "POST"
               :headers {"content-type" "application/json"}
               :body (write-json (->js doc))}))
      (.then #(.json %))))

(defn cat-indices!
  []
  (-> (js/fetch
        (str es-endpoint"/_cat/indices?format=json")
        (->js {:headers {"content-type" "application/json"}}))
      (.then #(.json %))
      (.then ->clj)))

(defn create-index!
  [index-name]
  (-> (js/fetch
        (str es-endpoint"/"index-name"?format=json")
        (->js {:method "put"
               :headers {"content-type" "application/json"}}))
      (.then #(.json %))
      (.then ->clj)))

(defn query-index
  [index-name query]
  (-> (js/fetch
        (str es-endpoint"/"index-name"/_search?format=json")
        (->js {:method "post"
               :body (write-json (->js query))
               :headers {"content-type" "application/json"}}))
      (.then #(.json %))
      (.then ->clj)))

(defn delete-doc!
  [index-name doc-id]
  (-> (js/fetch
        (str es-endpoint"/"index-name"/_doc/"doc-id)
        (->js {:method "delete"
               :headers {"content-type" "application/json"}}))
      (.then #(.json %))
      (.then ->clj)))

(def index-carto-numerique "carto-numerique")

(comment
  (deref indices)
 (do
   (def indices (atom nil))
   (-> (cat-indices!)
       (.then #(reset! indices %))))
(-> (create-index! "carto-numerique")
       (.then #(println %)))
 )
