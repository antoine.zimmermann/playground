(ns playground.carto.elasticsearcie
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]))


(def es-endpoint "http://localhost:9200")

(defn cat-indices
  []
  (let [response (client/get (str es-endpoint "/_cat/indices?format=json")
                             {:headers {"content-type" "application/json"}})]
    (update response :body #(json/read-str % :key-fn keyword))))

(defn create-index
  [index-name]
  (let [response (client/put (str es-endpoint "/" index-name "?format=json")
                             { :headers {"content-type" "application/json"}})]
    (update response :body #(json/read-str % :key-fn keyword))))

(defn post-doc!
  [index-name doc]
  (let [response (client/post (str es-endpoint "/" index-name "/_doc?format=json")
                             { :headers {"content-type" "application/json"}
                              :body (json/write-str doc)})]
    (update response :body #(json/read-str % :key-fn keyword))))

(defn query-index [index-name query]
  (let [response (client/post (str es-endpoint "/" index-name "/_search?format=json")
                              {:headers {"content-type" "application/json"}
                               :body (json/write-str query)})]
    (update response :body #(json/read-str % :key-fn keyword))))

(client/get "https://api.looksrare.org/graphql")
(json/read-str (get (:headers (ex-data *e)) "Report-To"))

(client/head (-> (json/read-str (get error "Report-To") :key-fn keyword) :endpoints first :url))
(def new-error (ex-data *e))
(:head)
(def error {"Server" "cloudflare",
  "Content-Type" "text/html; charset=UTF-8",
  "X-Content-Type-Options" "nosniff",
  "alt-svc"
  "h3=\":443\"; ma=86400, h3-29=\":443\"; ma=86400, h3-28=\":443\"; ma=86400, h3-27=\":443\"; ma=86400",
  "Permissions-Policy"
  "accelerometer=(),autoplay=(),camera=(),clipboard-read=(),clipboard-write=(),fullscreen=(),geolocation=(),gyroscope=(),hid=(),interest-cohort=(),magnetometer=(),microphone=(),payment=(),publickey-credentials-get=(),screen-wake-lock=(),serial=(),sync-xhr=(),usb=()",
  "X-Frame-Options" "SAMEORIGIN",
  "Strict-Transport-Security"
  "max-age=15552000; includeSubDomains; preload",
  "NEL"
  "{\"success_fraction\":0,\"report_to\":\"cf-nel\",\"max_age\":604800}",
  "Connection" "close",
  "Transfer-Encoding" "chunked",
  "Expires" "Thu, 01 Jan 1970 00:00:01 GMT",
  "Expect-CT"
  "max-age=604800, report-uri=\"https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct\"",
  "CF-RAY" "6cb86d32a8ce39f9-CDG",
  "Date" "Mon, 10 Jan 2022 19:49:05 GMT",
  "Vary" "Accept-Encoding",
  "Report-To"
  "{\"endpoints\":[{\"url\":\"https:\\/\\/a.nel.cloudflare.com\\/report\\/v3?s=nWje%2B8MG8DnDCOVB%2BtiIo3SlX7hqI5aImGtURaGA3UNZ8NbTeJKksqBbB%2FbMH8%2FKMvW2l%2BwAh1zDUg1K%2BUnbx1RuJc%2BeQb%2F198Of41m4%2FLOljlcrMv9Zvz1E%2FjFiCAxHmoQ1\"}],\"group\":\"cf-nel\",\"max_age\":604800}",
  "Cache-Control"
  "private, max-age=0, no-store, no-cache, must-revalidate, post-check=0, pre-check=0"})

(comment
  (create-index "yoloswag")
  (create-index "hackernews")
  (post-doc! "yoloswag" "{}")
  (post-doc! "yoloswag" {:lol "mdr"})

  (query-index "yoloswag" {})
  *e)
