(ns playground.youtube.core
  (:require [tools.react :as react :refer [defcomponent <> <fragment> <for> letcallback letstate set-state!]]
            [tools.react.utils :as rutils :refer [use-watch]]
            [cljs-bean.core :refer [->js ->clj bean]]
            ["@chakra-ui/react" :as ui]
            ["@chakra-ui/icons" :as icons]
            [tools.query :as q]))

(def YT_API_KEY "AIzaSyDe7ITGJ7lkWIR9IR4XfIL08BaQiYCC-MU")

(def YT_API_ENDPOINT "https://www.googleapis.com/youtube/v3")

(defonce query-client (q/create-query-client))

(defonce theme (ui/extendTheme (->js {})))

(comment
  (pprint/pprint (->clj theme)))

(defn use-search [query]
  (q/use-query #js ["search" query]
               (fn []
                 (-> (js/fetch (str YT_API_ENDPOINT "/search" "?key=" YT_API_KEY "&part=snippet" "&q=" query)
                               (->js {:method "GET"}))
                     (.then #(.json %))
                     (.then ->clj)))
               {:retry false
                :enabled (not (nil? (seq query)))
                :staleTime js/Infinity}))

(defcomponent ItemDetails
  [{:keys [item]}]
  (let [disclosure (ui/useDisclosure)]
    (<fragment>
     (<> ui/Button
         :variant "outline"
         :onClick #(rutils/on-toggle disclosure)
         "Details")
     (<> rutils/Print :data item :disclosure disclosure))))

(defcomponent SearchResultsList
  [{:keys [results on-play]}]
  (<fragment>
    (<> ui/List
        :spacing 10
        :mb "2rem"
        (<for> [item (:items results)
                :let [snippet (-> item :snippet)
                      thumbnail (let [{:keys [default medium high]} (:thumbnails snippet)]
                                  (or high medium default))]]
               (<> ui/ListItem :key (str (:id item))
                   (<> ui/Box :borderRadius "lg" :p 3 :borderColor "gray.200" :border "1px"
                       (<> ui/Container
                           :centerContent true
                           (<> ui/Heading :dangerouslySetInnerHTML #js {:__html (:title snippet)})
                           (when thumbnail
                             (<> ui/Box
                                 :position "relative"
                                 (<> ui/Image :src (:url thumbnail))
                                 (<> ui/Flex
                                     :top 0
                                     :left 0
                                     :position "absolute"
                                     :w "100%"
                                     :h "100%"
                                     :justifyItems "center"
                                     :alignItems "center")))
                           (<> ui/Text :mt 3 :fontSize "sm" (:description snippet))
                           (<> ui/Flex
                               :w "100%"
                               :justifyContent "space-between"
                               :pb 1
                               :pt 1
                               (<> ui/Button
                                   :colorScheme "teal"
                                   :variant "solid"
                                   :onClick #(on-play item)
                                   "Play")
                               (<> ItemDetails :item item)))))))))

(defcomponent SearchInput
  [{:keys [on-search]}]
  (letstate [search-query* ""]
    (letcallback [on-submit (fn [e]
                              (.preventDefault e)
                              (on-search @search-query*))
                  on-input (fn [e] (set-state! search-query* (-> e .-target .-value)))]
      (<> ui/Flex :as "form" :direction "column" :w "100%" :mt 5 :onSubmit on-submit
          (<> ui/Input :border"none" :placeholder "search a youtube video..." :color "white" :borderBottomRadius 0 :value @search-query* :onChange on-input)
          (<> ui/Button :borderWidth 1 :borderColor "blue.500" :borderBottomRadius "lg" :borderTopRadius "none" :type "submit":colorScheme "blue" "Search !")))))

(defonce state* (atom {}))

(defcomponent PlayerProvider
  [props]
  (letstate [is-ready* false]
    (let [toast (ui/useToast)]
      (react/run-effect [is-ready @is-ready*]
                        (fn []
                          (toast #js {:title "Youtube loaded"
                                      :duration 3000
                                      :isClosable true})
                          js/undefined))
      (react/run-effect :once
                        (fn []
                          (println "loading yt iframe")
                          (if-not (exists? js/YT)
                            (let [on-yt-iframe-ready (fn []
                                                       (set-state! is-ready* true))
                                  s (.createElement js/document "script")]
                              (set! (.-src s) "https://www.youtube.com/iframe_api")
                              (.appendChild (.-body js/document) s)
                              (set! (.-onYouTubeIframeAPIReady js/window) on-yt-iframe-ready)
                              (println "lol")
                              js/undefined)
                            (set-state! is-ready* true)))))
    (:children props)))

(defcomponent Youtube
  [props]
  (let [query* (use-watch state* :query)
        playing* (use-watch state* :playing)]
    (let [results* (use-search @query*)]
      (letcallback [on-search (fn [query]
                                (swap! state* assoc :query query))
                    on-play (fn [item]
                              (swap! state* assoc :playing item))]
        (<fragment>
          (<> ui/Container :maxW "xl" :centerContent true
              (<> SearchInput :on-search on-search)
              (if (empty? @query*)
                (<> ui/Heading "Please enter a new query")
                (<> ui/Heading @query*))
              (when-let [results (q/data results*)]
                (<> SearchResultsList
                    :results results
                    :on-play on-play)))
          (when-let [playing @playing*]
            (<> ui/Box
                :position "fixed"
                :bottom 0
                :backgroundColor "gray.700"
                :display "block"
                :w "100%"
                "hello world")))))))

(defcomponent App
  [props]
  (<> ui/ChakraProvider :theme theme
      (<> q/QueryClientProvider :client query-client
          (<> PlayerProvider
              (<> ui/Box :color "gray.100" :minHeight "100vh" :backgroundColor "gray.800"
                  (<> Youtube))))))
