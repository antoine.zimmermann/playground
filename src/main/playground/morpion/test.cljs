(ns playground.morpion.test
  (:require [tools.react :as react :refer [letstate do-once letcallback]]
            [tools.react.test :as rt]
            [cljs.test :as t]))

(defprotocol IMove
  :extend-via-metadata true
  (get-player [this] "Returns the player"))

(defprotocol IPosition
  :extend-via-metadata true
  (get-position [this] "returns a position a [x y]"))

(defprotocol IMorpion
  :extend-via-metadata true
  (get-board [game*] "Returns the board as a collection of IPosition values")
  (get-moves [game*] "Returns the moves played")
  (get-current-player [game*] "Returns the current player in #{:x :o}")
  (play! [game* ^IPosition position] "Play at position for current-player."))

(t/deftest
  morpion
  (t/testing
    "morpion game"
    (let [test* (rt/render-hook
                  (fn use-morpion
                    []
                    (letstate [game* {:current-player :x
                                      :moves ()}]
                      (letcallback [get-board-impl #(for [x (range 3)
                                                          y (range 3)]
                                                      (with-meta [x y] {`get-position identity}))
                                    get-moves-impl #(:moves @game*)
                                    get-current-player-impl #(:current-player @game*)
                                    play!-impl (fn [_ position]
                                                 (when-let [played-move (some
                                                                          #(= (get-position %) (get-position position))
                                                                          (get-moves game*))]
                                                   (throw (ex-info "Cannot play same move twice" {:position position
                                                                                                  :move played-move
                                                                                                  :error :morpion/already-played})))
                                                 (react/update-state! game*
                                                                      (fn [{:keys [current-player moves] :as game}]
                                                                        (assoc game
                                                                               :current-player (if (= :x current-player) :o :x)
                                                                               :moves (conj moves (with-meta [position (get-current-player game*)]
                                                                                                             {`get-position first
                                                                                                              `get-player second}))))))]
                        (do-once
                          (alter-meta! game* assoc
                                       `get-board get-board-impl
                                       `get-moves get-moves-impl
                                       `get-current-player get-current-player-impl
                                       `play! play!-impl)))
                      game*)))
          game* (rt/current test*)]
      (t/is (seq (get-board game*)) "a new game")
      (t/is (nil? (seq (get-moves game*))) "a new game does not have any moves played")
      (let [first-move-position (-> game* get-board first)
            first-player (get-current-player game*)]
        (t/is (= :x first-player) "First player is alwayx X")
        (rt/act
          (fn []
            (play! game* first-move-position)))
        (let [moves (get-moves game*)
              first-move (first moves)]
          (t/is (seq moves) "moves are non empty")
          (t/is (= (get-position (first moves))
                   (get-position first-move))
                "first move is at the correct position")
          (t/is (= first-player (get-player first-move))
                "first move was played by first player")
          (t/is (not= (get-current-player game*) first-player) "current player has been updated")
          (let [error (t/is (thrown? js/Error
                                     (play! game* first-move-position))
                            "Playing the same move twice throws an error")]
            (t/is (= :morpion/already-played (-> error ex-data :error))
                  "error can be identified"))
          ;; rerender after error
          (rt/rerender! test*)
          (t/is (= game* (rt/current test*)) "identity is stable accross rerenders")
          (let [available-positions (fn []
                                      (let [board (get-board game*)
                                            occupied (set (map get-position (get-moves game*)))]
                                        (filter #(not (occupied (get-position %)))
                                                board)))
                next-move (first (available-positions))]
            (t/is (not (nil? next-move))
                  "we can play another move"))))
      (rt/unmount test*))))
