(ns callbag.core
  (:require [cljs.test :refer-macros [deftest is testing]]
            [tools.react :as react :refer [<> defcomponent set-state! let-state let-ref]]
            [tools.react-dom :refer [create-root render!]]
            [tools.react.callbag :refer [use-source!]]
            [tools.callbag :refer [cb callbag deliver! greet! terminate!]]))


(defn start! [])

(defonce root (create-root "#main-container"))

(do
  (defn interval
    "returns a source that emits an event every t"
    [t]
    (cb :greet [sink]
        (let [interval-id (js/setInterval (fn [] (deliver! sink nil)) t)
              talkback (cb :terminate [e]
                           (js/clearInterval interval-id))]
          (greet! sink talkback))))

  (defn for-each
    "consumes a source, returns a callbag that can be used to terminate the operation"
    [source callback]
    (let [talkback* (atom nil)
          sink (cb :greet [talkback]
                   (do
                     (reset! talkback* talkback))
                   :deliver [data]
                   (callback data)
                   :terminate [error?]
                   (println "terminated"))]
      (greet! source sink)
      (if-let [talkback @talkback*]
        talkback
        (throw (ex-info "Sink was not greeted back. Handshake failed" {})))))

  (defcomponent CountDown
    [props]
    (let-state [counter (:initial-count props)]
      (let-ref [delta 10]
        (react/use-effect
          (fn []
            (let [terminate-for-each (for-each
                                       (interval (:time props))
                                       #(do
                                          (println "called" @counter @delta)
                                          (set-state! counter (- @counter @delta))))]
              (fn []
                (terminate! terminate-for-each))))
          #js [])
        (<> :div "Counter: " @counter))))

  (deftypecomponent Counter
    [props]
    IReactive
    (use! [this]
          (let-state [counter (:initial-count props)]
            {:count @counter}))
    IComponent
    (render [this display-data]
            (<> :div (:count display-data))))

  (render! root (<> CountDown
                    :initial-count 1000
                    :time 1000)))

(defn create-counter [initial-state]
  (let [state (use-state initial-state)
 , UserAdmin       event-bus (subject)]
    (map inc state-source)))

(defcomponent CallbagCountDown [props]
  (let-ref [talkback* nil
            sink (cb :greet [talkback]
                     (swap! talkback* talkback)
                     :deliver [new-state]
                     (set-state new-state))]
    (greet! callbag-countdown sink)
    (deliver! @talkback* props state)))

(defn inc! [counter]
  (deliver! counter nil))

(defn counter-ui [initial-state]
  (let [counter (create-counter initial-state)]
    (->> c
         (map (fn [value callback]
                (<> :div "count"
                    (<> :button :onClick (fn [e]
                                           (inc! counter)))))))))
