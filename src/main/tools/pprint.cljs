(ns tools.pprint
  (:require [tools.react :refer [<> defcomponent]]
            ["@chakra-ui/react" :as ui]))

(declare PPrint)

(defcomponent PPrintMap
  [{:keys [data]}]
  (<> ui/List
      :display "inline-block"
      :border "solid 1px red"
      (array (map (fn [[k v]]
                    (<> ui/ListItem
                        :key (str k)
                        (<> ui/Flex
                            :justify "space-between"
                            (<> ui/Text
                                (str k))
                            (<> ui/Text :flex "auto")
                            (<> PPrint :data v))))
                  data))))

(defcomponent PPrintList
  [{:keys [data]}]
  (<> ui/List
      :border "solid 1px blue"
      (array (map (fn [v i]
                    (<> ui/ListItem
                        :key i
                        :display "inline-block"
                        (<> PPrint :data v)))
                  data
                  (range)))))

(defcomponent PPrint
  [{:keys [data]}]
  (cond
    (map? data)
    (<> PPrintMap :data data)
    (or (vector? data) (list? data))
    (<> PPrintList :data data)
    :default
    (str data)))

