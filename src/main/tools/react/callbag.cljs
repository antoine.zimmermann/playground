(ns tools.react.callbag
  (:require [tools.callbag :refer [flat-map subject deliver! terminate! greet! subject as-source]]
            [tools.react :as react]))

(defn use-atom! [initial-state]
  (let [r (react/use-ref (atom initial-state))]
    (.-current r)))

(defn use-listen! [source initial-value]
  source)

(defn use-source!
  "source-fn returns a source that will we subscribed to"
  ([source-fn]
   (use-source! source-fn #js [])) 
  ([source-fn js-deps]
   (let [state* (use-atom! {:facade nil
                            :current-source nil})]
     (when-not (:facade @state*)
       ;; facade is a callbag that never changes and is returned by this hook.
       ;; whenever greeted it will look for the current source and greet it with the sink
       ;; if the source must change because its deps updated it will terminate it, generate a new
       ;; one and greet it with the original sink
       (swap! state* assoc
              :sinks #{}
              :facade (subject)
              :values (flat-map subject)))
     ;; whenever the deps change a new source must be recomputed
     ;; and delivered to the subject.
     ;; we expose a callbag that is our subject flattened so as to concat all the sources together
     (react/use-effect
       (fn []
         (let [new-source (as-source (source-fn))]
           (deliver! (:facade @state*) new-source)
           js/undefined))
       js-deps)
     (react/use-effect
       (fn []
         ;; we terminate the facade only when the component is about to be unmounted
         (fn []
           (terminate! (:facade @state*))))
       #js [])
     (:values @state*))))
