(ns tools.react.test
  (:require [cljs-bean.core :refer [bean ->clj]]
            ["@testing-library/react-hooks/dom/pure" :as test-hooks]
            ["@testing-library/react" :as testing-library]))



(defprotocol IComponentTestState
  (container [this])
  (base-element [this])
  (as-fragment [this]))

(defprotocol IHookTestStateResult
  (all [this])
  (current [this])
  (error [this])
  (wait-for-value-to-change [this] [this opt])
  (wait-for-next-update [this] [this opt]))

(defprotocol ITestState
  (rerender! [this] [this opt])
  (unmount [this])
  (cleanup [this])
  (add-cleanup [this cleanup!])
  (remove-cleanup [this]))

(defn get-base-element [container]
  (if (satisfies? IComponentTestState container)
    (base-element container)
    container))

; ==== get-by queries ====

(defn get-by-role
  ([container role]
   (get-by-role container #js {}))
  ([container role opt]
   (testing-library/getByRole (get-base-element container) role opt)))

(defn get-by-text
  ([container text]
   (get-by-text container text #js {}))
  ([container text opt]
   (testing-library/getByText (get-base-element container) text opt)))

(defn get-by-label-text
  ([container label-text]
   (get-by-label-text container label-text #js {}))
  ([container label-text opt]
   (testing-library/getByLabelText (get-base-element container) label-text opt)))

(defn get-by-placeholder-text
  ([container placeholder-text]
   (get-by-placeholder-text container placeholder-text #js {})) 
  ([container placeholder-text opt]
   (testing-library/getByPlaceholderText (get-base-element container) placeholder-text opt)))

(defn get-by-display-value
  ([container display-value]
   (get-by-display-value container display-value #js {})) 
  ([container display-value opt]
   (testing-library/getByDisplayValue (get-base-element container) display-value opt)))

(defn get-by-title
  ([container title]
   (get-by-title container title #js {})) 
  ([container title opt]
   (testing-library/getByTitle (get-base-element container) title opt)))

(defn get-by-test-id
  ([container test-id]
   (get-by-test-id container test-id #js {})) 
  ([container test-id opt]
   (testing-library/getByTestId (get-base-element container) test-id opt)))

; ==== find-by queries ====

(defn find-by-role
  ([container role]
   (find-by-role container #js {}))
  ([container role opt]
   (testing-library/findByRole (get-base-element container) role opt)))

(defn find-by-text
  ([container text]
   (find-by-text container text #js {}))
  ([container text opt]
   (testing-library/findByText (get-base-element container) text opt)))

(defn find-by-label-text
  ([container label-text]
   (find-by-label-text container label-text #js {}))
  ([container label-text opt]
   (testing-library/findByLabelText (get-base-element container) label-text opt)))

(defn find-by-placeholder-text
  ([container placeholder-text]
   (find-by-placeholder-text container placeholder-text #js {})) 
  ([container placeholder-text opt]
   (testing-library/findByPlaceholderText (get-base-element container) placeholder-text opt)))

(defn find-by-display-value
  ([container display-value]
   (find-by-display-value container display-value #js {})) 
  ([container display-value opt]
   (testing-library/findByDisplayValue (get-base-element container) display-value opt)))

(defn find-by-title
  ([container title]
   (find-by-title container title #js {})) 
  ([container title opt]
   (testing-library/findByTitle (get-base-element container) title opt)))

(defn find-by-test-id
  ([container test-id]
   (find-by-test-id container test-id #js {})) 
  ([container test-id opt]
   (testing-library/findByTestId (get-base-element container) test-id opt)))

; ===

(defrecord ComponentTestState
  [test-state]
  IComponentTestState
  (container [this]
    (.-container test-state))
  (base-element [this]
    (.-baseElement test-state))
  (as-fragment [this]
    (.asFragment test-state))
  ITestState
  (rerender! [this opt]
    (.rerender test-state opt))
  (unmount [this]
    (.unmount test-state)))

(defrecord HookTestState
  [test-state]
  IHookTestStateResult
  (all [this]
    (-> test-state .-result .-all ->clj))
  (current [this]
    (-> test-state .-result .-current))
  (error [this]
    (-> test-state .-result .-error ->clj))
  ITestState
  (rerender! [this]
    (.rerender test-state #js {}))
  (rerender! [this opt]
    (.rerender test-state opt))
  (unmount [this]
    (.unmount test-state)))

(defn render-component
  ([el]
   (render-component el #js {}))
  ([el opt]
   (->ComponentTestState (testing-library/render el opt))))

(defn render-hook
  "wrapper around @testing-library/react.renderHook. Returns a HookTestState."
  ([use-fn]
   (render-hook use-fn #js {}))
  ([use-fn opt]
   (->HookTestState (test-hooks/renderHook use-fn opt))))

(def act testing-library/act)
