(ns tools.react.utils
  (:require [cljs.pprint :as pprint]
            [tools.react :as react :refer [<> letstate run-effect set-state!]]
            ["@chakra-ui/react" :as ui]))

(defprotocol IChakraDisclosure
  :extend-via-metadata true
  (on-toggle [disclosure])
  (is-open [disclosure]))

(extend-protocol IChakraDisclosure
  default
  (on-toggle [native-disclosure]
    (-> native-disclosure .-onToggle (apply nil)))
  (is-open [native-disclosure]
    (.-isOpen native-disclosure)))

(defn use-watch
  "Hook to watch refs that implements INotify."
  ([iref] (use-watch iref identity))  
  ([iref selector]
   (letstate [out* (selector @iref)]
     (run-effect [iref iref]
                 (let [k #js {}]
                  (add-watch iref k (fn [_ _ _ new-state]
                                      (let [n (selector new-state)]
                                        (when (not= n @out*)
                                          (set-state! out* n)))))
                  (fn []
                    (remove-watch iref k))))
     out*)))

(react/defcomponent Print
                    [{:keys [data disclosure open]}]
                    (<> ui/Slide
                        :in (or open (when disclosure (is-open disclosure)))
                        :direction "bottom"
                        :style #js {:zIndex 1}
                        (<> ui/Box :as "pre" :backgroundColor "gray.700"
                            (<> ui/Box (<> ui/Button :variant "outline" :onClick #(on-toggle disclosure) "Close"))
                            (with-out-str (pprint/pprint data)))))
