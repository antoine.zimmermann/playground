(ns tools.query
  (:require [react-query :as rq :refer [useQuery useMutation useQueries]]
            [tools.react :as react :refer [letref letcallback do-once]]
            [cljs-bean.core :refer [bean ->js]]))

(defprotocol IQuery
  :extend-via-metadata true
  (data [this])
  (status [this])
  (error [this])
  (idle? [this])
  (loading? [this])
  (success? [this]))

(extend-protocol IQuery
  default
  (data [q] (:data q))
  (status [q] (:status q))
  (error [q] (:error q))
  (idle? [q] (:isIdle q))
  (loading? [q] (:isLoading q))
  (success? [q] (:isSuccess q)))

(defprotocol IFetch
  :extend-via-metadata true
  (stale? [this])
  (refetch! [this])
  (remove! [this])
  (fetched? [this]))

(defprotocol IMutation
  :extend-via-metadata true
  (mutate! [this args] [this args options])
  (mutate-async! [this args])
  (reset-mutation! [this]))

(extend-protocol IMutation
  default
  (mutate!
    ([m args] ((:mutate m) args))
    ([m args options] ((:mutate m) args (->js options))))
  (mutate-async! [m args]
    ((:mutateAsync m) args)))

(defn use-queries*
  [js-queries]
  (let [result-js-array (useQueries js-queries)]
    (map #(update (bean %) :val bean) (bean result-js-array))))

(defn use-query
  ([js-k query-fn]
   (use-query js-k query-fn nil))
  ([js-k query-fn options]
   (let [js-query (useQuery js-k query-fn (->js options))]
     ^{::query true
       `react/get-status :status}
     (bean js-query))))

(defn use-mutation
  ([mutation-fn]
   (use-mutation mutation-fn nil))
  ([mutation-fn options]
   (let [js-mutation (useMutation mutation-fn (->js options))]
     ^{::mutation true
       `react/get-status #(status %)}
     (bean js-mutation))))

(defn create-query-client []
  (rq/QueryClient.))

(def QueryClientProvider rq/QueryClientProvider)

(def use-query-client rq/useQueryClient)
