import { Component } from "react";

export class ErrorBoundary extends Component {
  constructor () {
    super();
    this.state = { error: null };
  }
  static getDerivedStateFromError (error) {
    return {
      error
    };
  }
  resetError () {
    return this.setState({ error: null });
  }
  componentDidCatch (e, info) {
    if (this.props.onError) {
      this.props.onError(e, info)
    }
  }
  render () {
    if (this.state.error) {
      const defaultHandle = () => "Error";
      const handle = this.props.handle || defaultHandle;
      return handle(this.state.error);
    }
    return this.props.children;
  }
}
