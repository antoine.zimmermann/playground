(ns tools.react
  (:require [react]
            [cljs-bean.core :as cljs-bean]
            [goog.object :as o]
            ["react-tools" :as rt])
  (:require-macros [tools.react :refer [<> <fragment> <provide> defcomponent letstate letref letcallback run-effect do-once]]))

(def sentinel #js {})

(def object-set (fn [o k v]
                     (o/set o k v)
                     o))

(def native-create-element react/createElement)

(def bean? cljs-bean/bean?)

(def bean cljs-bean/bean)

(def ->js cljs-bean/->js)

(def ->clj cljs-bean/->clj)

(def object cljs-bean/object)

(def use-callback react/useCallback)

(def use-state react/useState)

(def use-effect react/useEffect)

(def use-ref react/useRef)

(def use-memo react/useMemo)

(def create-context react/createContext)

(def use-context react/useContext)

(def Fragment react/Fragment)

(defn provider [context]
  (.-Provider context))

(defn consumer [context]
  (.-Consumer context))

(def ErrorBoundary rt/ErrorBoundary)

(defprotocol IState
  :extend-via-metadata true
  (set-state! [this state-or-fn]))

(defn update-state! [state update-fn & args]
  (set-state! state #(apply update-fn % args)))

(defprotocol ICljsWrapper
  (unwrap [this]))

(deftype ReactState [internal-state internal-set-state meta]
  IMeta
  (-meta [this] meta)
  IWithMeta
  (-with-meta [this new-meta]
    (ReactState. internal-state internal-set-state new-meta))
  IDeref
  (-deref [this]
    internal-state)
  IState
  (set-state! [this state-or-fn]
    (internal-set-state state-or-fn)))

(deftype ReactRef [^js internal-ref-value meta]
  IMeta
  (-meta [this] meta)
  IWithMeta
  (-with-meta [this new-meta]
    (ReactRef. internal-ref-value new-meta))
  IDeref
  (-deref [this]
    internal-ref-value)
  IReset
  (-reset! [this v]
    (set! (.-internal-ref-value this) v)
    @this)
  ISwap
  (-swap! [this f]
    (let [new-val (f @this)] (set! (.-internal-ref-value this) new-val) new-val))
  (-swap! [this f a]
    (let  [new-val (f @this a)] (set! (.-internal-ref-value this) new-val) new-val))
  (-swap! [this f a b]
    (let  [new-val (f @this a b)] (set! (.-internal-ref-value this) new-val) new-val))
 (-swap! [this f a b c]
    (let  [new-val (f @this a b c)] (set! (.-internal-ref-value this) new-val) new-val)))

(defprotocol IElement
  "Protocol to create React Elements.
  A default implementation is be provided that mirrors the React.createElement function."
  :extend-via-metadata true
  (create-element [component props-bean children]
                  "Mirror of React.createElement with fixed arity. This method is used by the <> macro to render elements.
                  component must be a valid React component, props are a cljs-bean object and children are a collection of React elements.
                  The default implementation applies the provided arguments to the React.createElement function, unwrapping the props."))

(extend-protocol IElement
  default
  (create-element [component props-bean children]
    (apply react/createElement
           component
           (let [props (if (bean? props-bean)
                         (object props-bean)
                         (let [props #{}]
                           (doseq [[k v] props-bean
                                   :let [ks (cond (keyword? k)
                                                  (str (when-let [n (namespace k)] n"/") (name k))
                                                  (symbol? k)
                                                  (str k)
                                                  :else
                                                  (do
                                                    (when-not (string? k)
                                                     (throw (ex-info (str "trying to pass"(type k)"that is not a valid props key (string, keyword or symbol)")
                                                                     {:props props-bean
                                                                      :key k})))
                                                    k))]]
                             (object-set props ks v))
                           props))]
             props)
           children)))

(defprotocol IStatus
  :extend-via-metadata true
  (get-status [this] "Returns the status of this. Generic protocol"))

(comment 
  (type (into (bean) {:value "b"}))
  (object (into (bean) (seq {:a "b"})))
  (type (merge (bean) {})))

(defmulti directives
  (fn [dir-key & args]
    dir-key))

(defmethod directives ::showprops
  [_ metadata]
  (fn [next-directive]
    (fn [component props children]
      (let [react-element (next-directive component props children)]
        react-element))))

(defn assoc-meta! [obj & kv]
  (apply alter-meta! obj assoc kv)
  obj)

(defn assoc-meta [obj & kv]
  (apply vary-meta obj assoc kv))

(comment
  (object (into (bean) {"color" "red"})))
