(ns tools.callbag
  (:require [clojure.spec.alpha :as s]
            [clojure.walk :refer [macroexpand-all]]))

(def signals #{:greet :deliver :terminate})

(def signals->code {:greet 0
                    :deliver 1
                    :terminate 2})

(s/def ::cb
       (s/cat :cb-name (s/? symbol?)
              :cb-body (s/+ (s/spec
                              (s/cat :signal signals
                                     :arg (s/? (s/spec (s/cat :identifier any?)))
                                     :body (s/* any?))))))

(do
  (defmacro cb [& body]
    (let [data (s/conform ::cb body)
          _ (when (= ::s/invalid data)
              (throw (ex-info "callbag: invalid" (s/explain-data ::cb body))))
          {:keys [cb-name cb-body]} data
          arg-sym (gensym "data")
          code `(fn [signal# ~arg-sym]
                  (case signal#
                    ~@(apply
                        concat
                        (map (fn [{:keys [arg body signal]}]
                               [(signals->code signal)
                                (if arg
                                  `(let [~(:identifier arg) ~arg-sym]
                                     ~@body)
                                  `(do
                                     ~@body))])
                             cb-body))
                    nil))]
      code)))

(comment
  (s/conform ::cb
             '((:greet [sink]
                       (println sink)
                       (println "lol"))))
  (macroexpand
    '(cb (:greet [sink]
                 (let [talkback (cb :terminate [e]
                                    (swap! sinks* disj sink))]
                   (swap! sinks* conj sink)
                   (greet! sink talkback)))
         (:deliver [data]
                   (do
                     (println "subject delivering")
                     (doseq [sink @sinks*]
                       (deliver! sink data))))
         (:terminate [e]
                     (do
                       (doseq [sink @sinks*]
                         (terminate! sink e))
                       (reset! sinks* #{})))))
  (macroexpand
    '(cb (:terminate [sink]
                          (let [interval-id (js/setInterval (fn [e]
                                                              (deliver! sink e))
                                                            t)
                                talkback (cb (:terminate [e]
                                                         (js/clearInterval interval-id)))]
                            (greet! sink talkback)))))
  (macroexpand
    '(cb (:greet [sink]
                 (println sink)))))
