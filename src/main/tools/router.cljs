(ns tools.router
  "React bindings for reitit.
  Router component is a react context that should be used at the root of the react tree. It broadcasts route state
  to its children matching the current state agains the reitit router passed as props.
  The Route component takes a :route prop that is an entry in the routing table of the Router. Route's does a partial / exact
  match against this target."
  (:require [tools.react :as react :refer [<> defcomponent <provide>]]
            [cljs-bean.core :refer [bean ->clj]]
            [reitit.core :as r]
            [reitit.frontend.easy :as rfe]
            [reitit.frontend :as rf]))

(def href rfe/href)

(def router rf/router)

(def match-by-path rf/match-by-path)

(def match-by-name rf/match-by-name)

(def RouterContext (react/create-context))

(defn use-router []
  (react/use-context RouterContext))

(def RouterProvider (react/provider RouterContext))

(def push-state rfe/push-state)

(defcomponent Router
  [{:keys [children router] :as props}]
  (let [Provider RouterProvider
        [router-context set-router-context] (react/use-state {:started false
                                                              :router router
                                                              :match nil
                                                              :history nil})]
    (react/use-effect
      (fn []
        (rfe/start! router
                    (fn on-navigate [match history]
                      (set-router-context (fn [router-context]
                                            (assoc router-context
                                                   :started true
                                                   :match match
                                                   :history history))))
                    {:use-fragment false})
        (fn []
          (comment
            (println "unmounting router"))))
      #js [])
    (<provide> RouterContext
               :value router-context
               (<> react/Fragment
                   children))))

(defcomponent Route
  [{:keys [children route exact] :as props}]
  (let [router-context (use-router)
        {:keys [match router started]} router-context
        ;; only do exact match
        should-render? (when (and match started)
                         (and match (= route (-> match :data :name))))]
    (when should-render?
      (<> RouterProvider
          :value (assoc router-context
                        :route route)
          (<> react/Fragment
              children)))))

(defcomponent RouteNotFound
  [{:keys [children] :as props}]
  (let [router-context (use-router)
        {:keys [match router]} router-context]
    (<> react/Fragment
        (when-not (seq match)
          children))))

(defcomponent RouteIndex
  [{:keys [children]}]
  (let [{:keys [router match route]} (use-router)]
    (if (= route (-> match :data :name))
      (<> react/Fragment
          children))))

(defcomponent Link
  [{:keys [children className to] :as props}]
  (<> :a
      :href to
      :onClick (fn [e]
                 (.preventDefault e))
      :className className
      children))
