(ns tools.callbag.test
  (:require [cljs.test :refer-macros [deftest is testing async] :refer [test-vars]]
            [tools.callbag :refer [callbag flat-map subject greet! terminate! deliver! from-vector listen! as-source]]
            [tools.react :as react]
            [tools.react.test :as t]
            [tools.react-dom]))
(do

  (defn get-current [test-state]
    (.-current test-state))

  (defn use-render-counter []
    (let [counter* (react/use-ref 0)]
      (set! (.-current counter*) (inc (get-current counter*)))
      (println "render count: " (get-current counter*))
      (get-current counter*)))

  (defn use-timeout [duration callback]
    (react/use-effect
      (fn []
        (println "resetting timer with duration " duration)
        (let [id (js/setTimeout
                   (fn []
                     (callback))
                   duration)]
          (fn []
            (js/clearTimeout id))))
      #js [duration]))

  (deftest timer
    (testing "callback has been called after duration"
      (async done
             (let [duration 15
                   called? (atom false)
                   create-callback (fn [callback]
                                     (if-not @called?
                                       (do
                                         (reset! called? true)
                                         (callback))
                                       (throw (ex-info "cannot call callback twice" {}))))
                   callback (create-callback (fn []
                                               (println "called")))
                   test-state (t/render-hook
                                #(let [render-counter (use-render-counter)
                                       _ (use-timeout duration callback)]
                                   {:render-counter render-counter})
                                #js {:initialProps #js {:duration duration}})
                   current (t/current test-state)]
               (is (= 1 (:render-counter current)) "there was 1 render")
               (js/setTimeout #(do
                                 (is (= 2 (-> test-state t/current :render-counter))
                                     "there was another render after the time")
                                 (done))
                              (inc duration))
               test-state)))
    (comment "timer will stop if nil is passed as duration"
             (async done
                    (let [duration 10
                          half-time 5
                          test-state (t/render-hook #(let [render-counter (use-render-counter)
                                                           _ (use-timeout duration)]
                                                       {:render-counter render-counter}))
                          current (t/current test-state)]
                      (is (= 1 (:render-counter current)) "there was 1 render")
                      (js/setTimeout #(do
                                        (t/rerender test-state)
                                        (is (= 2 (-> test-state t/current :render-counter))
                                            "there was another render after the time"))
                                     half-time)
                      (js/setTimeout #(do
                                        (is (= 3 (-> test-state t/current :render-counter))
                                            "there was another render after the time")
                                        (done))
                                     (inc duration))
                      test-state)))
    (comment "timer will not reset if there is a rerender"
             (async done
                    (let [duration 10
                          half-time 5
                          test-state (t/render-hook #(let [render-counter (use-render-counter)
                                                           _ (use-timeout (.-duration %))]
                                                       {:render-counter render-counter})
                                                    #js {:initialProps #js {:duration duration}})
                          current (t/current test-state)]
                      (is (= 1 (:render-counter current)) "there was 1 render")
                      (js/setTimeout #(do
                                        (t/rerender test-state)
                                        (is (= 2 (-> test-state t/current :render-counter))
                                            "there was another render after the time"))
                                     half-time)
                      (js/setTimeout #(do
                                        (is (= 3 (-> test-state t/current :render-counter))
                                            "there was another render after the time")
                                        (done))
                                     (inc duration))
                      test-state))))

  (comment
    (test-vars [#'tools.callbag.test/timer])))

(deftest test-2
  (testing "flat-map"
    (let [s (subject)
          source (flat-map s)
          test-state (atom {})
          sink (callbag :on-greet (fn [talkback]
                                    (swap! test-state assoc :talkback talkback))
                        :on-deliver (fn [data]
                                      (swap! test-state update :values conj data)))]
      (greet! source sink)
      (deliver! s (from-vector (mapv identity (range 10))))
      (is (= (reverse (range 10)) (:values @test-state)))
      (deliver! s (from-vector (mapv identity (range 10))))
      (is (= (concat
               (reverse (range 10))
               (reverse (range 10)))
             (:values @test-state)))
      test-state)))

(deftest test-3
 (testing "subject"
   (let [s (subject)
         test-state (atom {})
         sink (callbag :on-greet (fn [talkback]
                                   (swap! test-state assoc :talkback talkback))
                       :on-deliver (fn [data]
                                     (swap! test-state update :values conj data)))]
     (greet! s sink)
     (deliver! s 1)
     (deliver! s 2)
     (is (= '(2 1) (:values @test-state)))
     (terminate! (:talkback @test-state))
     (deliver! s 3)
     (is (= '(2 1) (:values @test-state)))
     test-state)))

(deftest test-4
 (testing "converting an atom to a listenable source"
   (let [counter (atom 0)
         increase-counter (fn []
                            (swap! counter inc))
         test-state (atom {:greeted? false
                           :talkback nil
                           :values ()})
         source (as-source counter)
         sink (callbag
                :on-greet (fn [talkback]
                            (swap! test-state assoc
                                   :greeted? true
                                   :talkback talkback))
                :on-deliver (fn [data]
                              ;; store the values provided by the source in our test state
                              (swap! test-state update :values conj data)))]
     (testing "greeting the source"
       (greet! source sink)
       (is (:greeted? @test-state) "source has greeted the sink")
       (is (fn? (:talkback @test-state)) "source has provided a talkback to the sink")
       (is (= @counter (-> @test-state :values first)) "source has provided an initial value to the sink"))
     (testing "swapping the atom's value"
       (increase-counter)
       (is (= @counter (-> @test-state :values first)) "the new value has been dispatched")
       (increase-counter)
       (is (= @counter (-> @test-state :values first)) "the new value has been dispatched"))
     (testing "terminating the source using the talkback"
       (let [delivered-values (:values @test-state)
             talkback (:talkback @test-state)]
         (terminate! talkback)
         (increase-counter)
         (is (= (:values @test-state)
                delivered-values)
             "No new values were delivered to the sink")))
     test-state)))


(deftest test-5
 (testing "converting a vector to a pullable source"
   (let [input (mapv identity (range 2))
         test-state (atom {:greeted? false
                           :talkback nil
                           :sink-terminated? false
                           :values ()})
         source (as-source input)
         sink (callbag
                :on-greet (fn [talkback]
                            (swap! test-state assoc
                                   :greeted? true
                                   :talkback talkback))
                :on-deliver (fn [data]
                              ;; store the values provided by the source in our test state
                              (swap! test-state update :values conj data))
                :on-terminate (fn []
                                (swap! test-state assoc :sink-terminated? true)))]
     (testing "greeting the source"
       (greet! source sink)
       (is (:greeted? @test-state) "source has greeted the sink")
       (is (fn? (:talkback @test-state)) "source has provided a talkback to the sink"))
     (testing "pulling data from the source"
       (let [talkback (:talkback @test-state)]
         (deliver! talkback nil)
         (is (= (first input) (-> @test-state :values first)) "the new value has been dispatched")))
     (testing "exhausting the source should terminate the sink"
       (let [delivered-values (:values @test-state)
             talkback (:talkback @test-state)]
         (is (not (:sink-terminated? @test-state)))
         (deliver! talkback nil) 
         (is (= (:values @test-state)
                (reverse input))
             "The input has been deliverd as a whole")
         (is (:sink-terminated? @test-state))))
     test-state)))


(deftest test-1
 (testing "creating a sink from an atom to pull a source"
   (let [test-state (atom {:greeted? false
                           :talkback nil
                           :sink-terminated? false
                           :values ()})
         counter (atom 0)
         source (as-source counter)
         increment! #(swap! counter inc)
         atom-sink (atom nil)
         handle (listen! atom-sink source)]
     (testing "source has pushed the initial value"
       (is (fn? handle))
       (is (= @counter @atom-sink)))
     (testing "sink listens for additional values"
       (doseq [i (range 10)]
         (increment!)
         (is (= @counter @atom-sink) "sinks listens for value")))
     (testing "terminating the handle terminates the source"
       (is (= @counter @atom-sink))
       (terminate! handle)
       (increment!)
       (is (= (dec @counter) @atom-sink)))
     atom-sink)))
