(ns tools.callbag
  (:require [cljs.test :refer-macros [deftest is testing]])
  (:require-macros [tools.callbag :refer [cb]]))


(defprotocol ICallbag
  :extend-via-metadata true
  (deliver! [callbag data])
  (greet! [callbag greeter])
  (terminate! [callbag] [callbag error]))

(extend-protocol ICallbag
  function
  (greet! [f greeter]
    (f 0 greeter))
  (deliver! [f data]
    (f 1 data))
  (terminate!
    ([f]
     (f 2))
    ([f error]
     (f 2 error))))

(defprotocol ICallbagSource
  :extend-via-metadata true
  (as-source [this] [this opt] "Returns a source callbag"))

(defprotocol ICallbagConsumer
  :extend-via-metadata true
  (pull! [this source])
  (listen! [this source]))

(extend-protocol ICallbagSource
  function
  (as-source [this]
    this)
  Atom
  (as-source [this]
    (cb (:greet [sink]
                ; we greet the sink back wiht a talkback. this talkback is used by the sink to interrupt the data flow
                (let [k #js {}
                      talkback (cb (:terminate [v]
                                               (remove-watch this k)))
                      watch-fn (fn [k r old-value new-value]
                                 (deliver! sink new-value))]
                  (add-watch this k watch-fn)
                  (greet! sink talkback)
                  ; immediately deliver initial data to the sink
                  (deliver! sink @this)))))
  cljs.core/PersistentVector
  (as-source [this]
    (cb (:greet [sink]
                (let [values* (volatile! (seq this))
                      talkback (cb (:deliver [err] (let [values @values*
                                                        next-value (first values)]
                                                    (deliver! sink next-value)
                                                    (if-let [values (next values)]
                                                      (vreset! values* values)
                                                      (terminate! sink)))))]
                  (greet! sink talkback))))))

(extend-protocol ICallbagConsumer
  Atom
  (listen! [this source]
    "listen to the source for values"
    (let [talkback* (atom nil)
          handle (cb
                   (:terminate [e]
                               (terminate! @talkback* e)))
          sink (cb
                 (:greet [talkback]
                             (reset! talkback* talkback))
                 (:deliver [data]
                           (reset! this data)))]
      (greet! source sink)
      handle)))

(defn from-vector [v]
  (cb (:greet [sink]
                       (let [running? (atom true)
                             talkback (cb (:terminate 
                                            (reset! running? false)))]
                         (greet! sink talkback)
                         (loop [v v]
                           (when (and @running? v)
                             (do
                               (deliver! sink (first v))
                               (recur (next v)))))))))


(defn subject []
  (let [sinks* (atom #{})]
    (cb (:greet [sink]
                (let [talkback (cb (:terminate [e]
                                               (swap! sinks* disj sink)))]
                  (swap! sinks* conj sink)
                  (greet! sink talkback)))
        (:deliver [data]
                  (do
                    (println "subject delivering")
                    (doseq [sink @sinks*]
                      (deliver! sink data))))
        (:terminate [e]
                    (do
                      (doseq [sink @sinks*]
                        (terminate! sink e))
                      (reset! sinks* #{}))))))


(defn scan [source scan-fn initial-value]
  (cb (:greet [sink]
              (let [value* (atom initial-value)
                    sink' (cb (:greet [talkback]
                                      (greet! sink talkback)
                                      (deliver! sink initial-value))
                              (:deliver [data]
                                        (deliver! sink (swap! value* scan-fn data))))]
                (greet! source sink')))))

(defn flat-map
  [outer-source]
  (cb
    (:greet [sink]
                (let [talkbacks* (atom {})
                      talkback (cb (:terminate
                                        [e]
                                          (when-let [inner (:inner @talkbacks*)]
                                            (terminate! inner e))
                                          (when-let [outer (:outer @talkbacks*)]
                                            (terminate! outer e))))]
                  (greet! outer-source
                          ;; inner sink
                          (cb
                            (:greet [outer-talkback]
                                        (swap! talkbacks* assoc :outer outer-talkback)
                                        (greet! sink talkback))
                            (:deliver [inner-source]
                                          ;; when the outer source dispatches a new source we terminate the previous one
                                          ;; if there was one
                                          (when-let [previous-inner-talkback (:inner @talkbacks*)]
                                            (terminate! previous-inner-talkback))
                                          ;; and we greet the new one
                                          (greet! inner-source
                                                 (cb
                                                   (:greet [inner-talkback]
                                                               (swap! talkbacks* assoc :inner inner-talkback)
                                                               ;; immediately request data from the inner source
                                                               (deliver! inner-talkback nil))
                                                   (:deliver [data]
                                                                 ;; when the inner source delivers data, pass it to the sink
                                                                 (deliver! sink data))
                                                   ;; when the inner source terminates, request data from the outer source
                                                   (:terminate [data]
                                                                   (when-let [outer-talkback (:outer @talkbacks*)]
                                                                     (deliver! outer-talkback nil))
                                                                   (swap! talkbacks* assoc :inner nil)))))))))))


(defn fmap [fmap-fn callbag-source]
  (cb
    (:greet [sink]
            (greet! callbag-source (cb (:greet [talkback]
                                               (greet! sink talkback))
                                       (:deliver [data]
                                                 (let [data' (fmap-fn data)]
                                                   (deliver! sink data'))))))))

(defn only [only-fn callbag-source]
  (cb (:greet [sink]
              (greet! callbag-source (cb (:greet [talkback]
                                                 (greet! sink talkback))
                                         (:deliver [data]
                                                   (when (only-fn data)
                                                     (deliver! sink data))))))))

(defn tap [tap-fn callbag-source]
  (cb (:greet [sink]
              (greet! callbag-source (cb (:greet [talkback]
                                                 (greet! sink talkback))
                                         (:deliver [data]
                                                   (do
                                                     (tap-fn data)
                                                     (deliver! sink data))))))))

(defn interval [t]
  (cb (:greet [sink]
              (when-not sink
                (throw (ex-info "source greeted without sink" {:error :empty-greet})))
              (let [interval-id (js/setInterval #(deliver! sink nil) t)
                    talkback (cb (:terminate (js/clearInterval interval-id)))]
                (greet! sink talkback)))))

(defn for-each
  "consumes a source, returns a callbag that can be used to terminate the operation"
  [source callback]
  (let [talkback* (atom nil)
        sink (cb (:greet [talkback]
                         (do
                           (reset! talkback* talkback)))
                 (:deliver [data]
                           (callback data)))]
    (greet! source sink)
    (fn []
      (terminate! @talkback*))))
