(ns tools.react-dom
  (:require ["react-dom" :as react-dom]))

(defprotocol IRoot
  (render! [root component]))

(deftype Root [container]
  IRoot
  (render! [root react-element]
    (.render react-dom react-element container)))

(defn create-root [selector]
  (let [container (.querySelector js/document selector)]
    (when (not container)
      (throw (ex-info
               (str "Selector did not match any alement: " selector)
               {})))
    (Root. container)))
