(ns tools.react
  (:require [clojure.spec.alpha :as s]
            [clojure.walk :refer [macroexpand-all]]))

(s/def ::prop (s/cat :prop-key keyword?
                      :prop-val any?))

(s/def ::props (s/* ::prop))

(s/def ::element-description
  (s/cat :component any?
         :props ::props
         :children (s/* any?)))

(defmacro <> [& args]
  (let [element-description (s/conform ::element-description args)]
    (if (s/invalid? element-description)
      (throw (ex-info "Invalid react element" (s/explain-data ::element-description args)))
      (let [component (let [component (:component element-description)]
                        (cond
                          (keyword? component)
                          (name component)
                          :default
                          component))
            props (reduce
                    (fn [props {:keys [prop-key prop-val]}]
                      (cond
                        (= :... prop-key)
                        `(into ~props ~prop-val)
                        (= :...js prop-key)
                        `(into ~props (tools.react/bean ~prop-val))
                        :else
                        `(assoc ~props ~prop-key ~prop-val)))
                    `(tools.react/bean)
                    (:props element-description))
            children (:children element-description)]
        `(tools.react/create-element ~component ~props ~children)))))

(defmacro <fragment> [& args]
  `(tools.react/<> tools.react/Fragment ~@args))

(comment
  (macroexpand-all
    '(<> RouterContext
         :... props
         :value router-context
         (<> react/Fragment
             children)))
  (macroexpand-all
    `(<> :div "helloworld"
         (<> Hello))))

(defmacro <provide> [context & body]
  `(tools.react/<> (.-Provider ~context) ~@body))


(comment
  (macroexpand-all
    '(<provide> token-context
                :value {:token @token*
                        :set-token #(react/set-state! token* %)}
                (<fragment> children))))


(defmacro <context> [context & body]
  `(tools.react/<> (.-Provider ~context) ~@body))

(defmacro defcomponent [comp-name props-bindings & body]
  `(defn ~comp-name
     [props#]
     (let [props# (tools.react/bean props#)
           ~(first props-bindings) props#]
       ~@body)))

(defmacro <>for [& body]
  `(tools.react/<> tools.react/Fragment
                   (apply clojure.core/array (for ~@body))))

(defmacro <for> [& body]
  `(tools.react/<> tools.react/Fragment
                   (apply clojure.core/array (for ~@body))))

(do
  (s/def ::binding (s/cat :identifier any?
                          :expression any?))

  (s/def ::let
         (s/cat :bindings-vec (s/spec
                                (s/cat
                                  :bindings (s/+ ::binding)))
                :body (s/* any?)))


  (s/def ::let?
         (s/cat :bindings-vec (s/?
                                (s/spec
                                  (s/cat
                                    :bindings (s/+ ::binding))))
                :body (s/* any?)))

  (defmacro letstate [& code]
    (let [spec (s/conform ::let code)]
      (when (= ::s/invalid spec)
        (throw (ex-info "letstate invalid code" (s/explain-data ::let code))))
      (let [bindings (->> spec
                          :bindings-vec
                          :bindings
                          reverse
                          (reduce
                            (fn [body {:keys [identifier expression]}]
                              `(let [[state# set-state#] (use-state ~expression)
                                     state-ref# (use-ref nil)
                                     ~identifier (if-let [current-state# ^js (.-current state-ref#)]
                                                   (do
                                                     (set! (.-internal-state current-state#) state#)
                                                     (set! (.-internal-set-state current-state#) set-state#)
                                                     current-state#)
                                                   (let [current-state# (new ReactState state# set-state# nil)]
                                                     (set! (.-current state-ref#) current-state#)
                                                     current-state#))]
                                 ~body))
                            `(do
                               ~@(:body spec))))]
        bindings)))

  (defmacro letref [& code]
    (let [spec (s/conform ::let code)]
      (when (= ::s/invalid spec)
        (throw (ex-info "letref invalid code" (s/explain-data ::let code))))
      (let [bindings (->> spec
                          :bindings-vec
                          :bindings
                          reverse
                          (reduce
                            (fn [body {:keys [identifier expression]}]
                              `(let [~identifier (.-current (use-ref (new ReactRef :react/init nil)))]
                                 (when (= :react/init (deref ~identifier))
                                   (reset! ~identifier ~expression))
                                 ~body))
                            `(do
                               ~@(:body spec))))]
        bindings)))

  (defmacro run-effect [flag-or-bindings & code]
    (let [[effect-deps-type deps] (if (#{:once :always} flag-or-bindings)
                                    [:flag flag-or-bindings]
                                    (let [bindings (s/conform (s/cat :bindings (s/+ ::binding)) flag-or-bindings)]
                                      (if (= ::s/invalid bindings)
                                        (throw (ex-info "could not parse bindings" (s/explain-data ::binding flag-or-bindings)))
                                        [:bindings bindings])))
          js-deps (case effect-deps-type
                    :flag (case deps
                            :once `(clojure.core/array)
                            :always nil)
                    :bindings )]
      (case effect-deps-type
        :flag (case deps
                :once `(tools.react/use-effect
                         (fn []
                           ~@code)
                         (clojure.core/array))
                :always `(tools.react/use-effect
                           (fn []
                             ~@code)))
        :bindings (let [bindings (reduce
                                   (fn [bindings {:keys [identifier expression]}]
                                     (conj bindings
                                           identifier expression))
                                   []
                                   (:bindings deps))]
                    `(let ~bindings
                       (tools.react/use-effect
                         (fn []
                           ~@code)
                         (clojure.core/array ~@(->> deps :bindings (map :identifier)))))))))


  (defmacro letcallback [& code]
    (let [spec (s/conform ::let code)]
      (when (= ::s/invalid spec)
        (throw (ex-info "letref invalid code" (s/explain-data ::let code))))
      (let [bindings (->> spec
                          :bindings-vec
                          :bindings
                          reverse
                          (reduce
                            (fn [body {:keys [identifier expression]}]
                              (let [refsym (gensym "ref")]
                                `(letref [~refsym nil
                                           cb# (fn [& args#] (apply (deref ~refsym) args#))]
                                   (reset! ~refsym ~expression)
                                   (let [~identifier (deref cb#)]
                                     ~body))))
                            `(do
                               ~@(:body spec))))]
        bindings))))

(defmacro do-once
  "Executes body once in the component lifetime"
  [& body]
  `(letref [r# tools.react/sentinel]
     (when (= tools.react/sentinel)
       (reset! r# (do ~@body)))
     @r#))

(defn render-bindings [bindings body]
  (let [bindings (->> bindings
                      reverse
                      (reduce
                        (fn [body {:keys [identifier expression]}]
                          (let [refsym (gensym "ref")]
                            `(letref [~refsym nil
                                       cb# (fn [& args#] (apply (deref ~refsym) args#))]
                               (reset! ~refsym ~expression)
                               (let [~identifier (deref cb#)]
                                 ~body))))
                        body))]
    bindings))
