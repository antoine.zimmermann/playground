(ns notebook.day2
  (:require [clojure.string :as string]))


(def test-input (let [f (slurp "src/main/notebook/day2.test.txt")]
                  (partition 2 (read-string (str "("f")")))))

(def input-day-2 (let [f (slurp "src/main/notebook/day2.input.txt")]
                  (partition 2 (read-string (str "("f")")))))
(do
  (defn answer-1
    [input]
    (let [[xf yf] (reduce
                    (fn [[x y] [direction dist]]
                      (cond
                        (= 'forward direction) [(+ x dist) y]
                        (= 'down direction) [x (+ y dist)]
                        (= 'up direction) [x (- y dist)]
                        :else [x y]))
                    [0 0]
                    input)]
      (* xf yf))))

(answer-1 input-day-2)



(do
  (defn answer-2
    [input]
    (let [[xf yf] (reduce
                    (fn [[x y aim] [direction dist]]
                      (cond
                        (= 'forward direction) [(+ x dist) (+ y (* aim dist)) aim]
                        (= 'down direction) [x y (+ aim dist)]
                        (= 'up direction) [x y (- aim dist)]))
                    [0 0 0]
                    input)]
      (* xf yf)))
  (answer-2 input-day-2))
