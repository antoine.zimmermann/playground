(ns notebook.day4
  (:require [clojure.string :as string]))

(defn load-input [filename]
  (let [file (slurp filename)
        lines (string/split file #"\n")
        [draw-order & lines] lines
        bloc (atom 0)]
    {:draw-order (map read-string (string/split draw-order #","))
     :bingo-boards (->> lines
                        (filter #(not= "" %))
                        (map #(re-seq #"\d+" %))
                        (map #(map read-string %))
                        (partition 5))}))

(def test-input (load-input "src/main/notebook/day4.test.input"))

(def input (load-input "src/main/notebook/day4.input"))

(do

  (defn create-bingo-board [board]
    (let [bingo-board {:board board
                       ;; used to check if the number has a coord in the board
                       :number->coord (reduce
                                        (fn [board-contents [board-row row-index]]
                                          (reduce
                                            (fn [board-contents [board-number col-index]]
                                              (assoc board-contents board-number [row-index col-index]))
                                            board-contents
                                            (map vector
                                                 board-row
                                                 (range))))
                                        {}
                                        (map vector
                                             board
                                             (range)))}]
      (assoc bingo-board
             :size (count (first (:board bingo-board))))))

  (defn play [bingo-board number-drawn]
    (let [coords (get (:number->coord bingo-board) number-drawn)
          update-count #(if-not % 1 (inc %))]
      (if-let [[row col] coords]
        (-> bingo-board
            (update-in [:counts :row row] update-count)
            (update-in [:counts :col col] update-count)
            (update :played conj number-drawn))
        bingo-board)))

  (defn winner? [bingo-board]
    (let [win? (fn [[idx total]]
                 (= total (:size bingo-board)))]
      (or
        (some win? (-> bingo-board :counts :row))
        (some win? (-> bingo-board :counts :col)))))

  (defn score [bingo-board]
    (let [played-numbers (set (:played bingo-board))
          unmarked-numbers (filter #(not (played-numbers %)) (keys (:number->coord bingo-board)))
          unmarked-sum (reduce + unmarked-numbers)]
      (* (first (:played bingo-board))
         unmarked-sum)))

  (defn final-score [input]
    (reduce
      (fn [bingo-boards number-drawn]
        (let [bingo-boards (map (fn [bingo-board]
                                  (play bingo-board number-drawn))
                                bingo-boards)]
          (if-let [winner (first (filter winner? bingo-boards))]
            (reduced (score winner))
            bingo-boards)))
      (map create-bingo-board (:bingo-boards input))
      (:draw-order input)))

  (defn latest-score [input]
    (reduce
      (fn [bingo-boards number-drawn]
        (let [bingo-boards (map (fn [bingo-board]
                                  (play bingo-board number-drawn))
                                bingo-boards)
              losers (filter #(not (winner? %)) bingo-boards)]
          (if-not (seq losers)
            (reduced (score (first bingo-boards)))
            losers)))
      (map create-bingo-board (:bingo-boards input))
      (:draw-order input)))

  (latest-score test-input)
  (latest-score input))
