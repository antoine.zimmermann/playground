(ns notebook.day3
  (:require [clojure.string :as string]))


(defn load-input [filename]
  (map (fn [line] (mapv read-string (string/split line #"")))
       (string/split (slurp filename) #"\n")))

(def test-input (load-input "src/main/notebook/day3.test.input"))

(def input (load-input "src/main/notebook/day3.input"))

(do
 (defn inverse [report]
   (mapv #(case %
            0 1
            1 0
            %)
         report))

 (defn pow2 [n]
   (reduce *' (repeat n 2)))

 (defn decimal [word]
   (reduce
     (fn [total [w p]]
       (+ total (* w (pow2 p))))
     0
     (map vector (reverse word) (range))))

 (defn power-consumption [input]
   (let [mean (reduce
                (fn [out report]
                  (mapv + out report))
                (map
                  (fn [report]
                    (mapv #(case %
                             1 1
                             0 -1)
                          report))
                  input))
         report (mapv #(cond
                         (> % 0) 1
                         :else 0)
                      mean)
         gamma (decimal report)
         epsilon (decimal (inverse report))
         consumption (* gamma epsilon)]
     [mean report gamma epsilon consumption]))
 )

(* oxygen-gen-rate co2-scrub-rate)

(comment
  "anser test input"
  (power-consumption test-input)
  (power-consumption input))

(do
  (defn oxygen-rating-bit [input bit-index]
    (let [[mean] (power-consumption input)
          target (first mean)
          mask (fn [bit-index]
                 (cond
                   (= target 0) 1
                   (> target 0) 1
                   :else 0))
          binary-oxygen-rating (reduce
                                 (fn [remaining-numbers bit-index]
                                   (if (next remaining-numbers)
                                     (filter
                                       #(= (mask bit-index)
                                           (get % bit-index))
                                       remaining-numbers)
                                     (reduced (first remaining-numbers))))
                                 input
                                 (range (count mean)))]
      [mean binary-oxygen-rating (decimal binary-oxygen-rating)]))

  (defn oxygen-rating-mask
    "returns a bit mask"
    [input]
    (let [[mean] (power-consumption input)]
      (mapv #(cond
               (>= % 0) 1
               :else 0)
            mean)))

  (defn co2-rating-mask
    "returns a bit mask"
    [input]
    (let [[mean] (power-consumption input)]
      (println mean)
      (mapv #(cond
               (>= % 0) 0 
               :else 1)
            mean)))

  (defn filter-numbers [mask numbers bit-index]
    (let [bit #(get % bit-index)]
      (filter
        (fn [number]
          (= (bit number) (bit mask)))
        numbers)))

  (defn oxygen-rating-bit
    [input]
    (first
      (reduce
        (fn [numbers bit-index]
          (let [mask (oxygen-rating-mask numbers)]
            (if (next numbers)
              (filter-numbers mask numbers bit-index)
              (reduced numbers))))
        input
        (range (count (first input))))))

  (defn co2-rating-bit
    [input]
    (first
      (reduce
        (fn [numbers bit-index]
          (println numbers)
          (let [mask (co2-rating-mask numbers)]
            (println mask)
            (if (next numbers)
              (filter-numbers mask numbers bit-index)
              (reduced numbers))))
        input
        (range (count (first input))))))

  (defn oxygen-rating [input]
    (decimal (oxygen-rating-bit input)))

  (defn co2-rating [input]
    (decimal (co2-rating-bit input)))

  (let [o2 (oxygen-rating input)
        co2 (co2-rating input)]
    [o2
     co2
     (* o2 co2)]))
