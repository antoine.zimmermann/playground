(ns notebook.core
  (:require [clojure.string :as string]))

(def day1-test-input (->> (string/split (slurp "src/main/notebook/day1.test.txt") #"\n")
                          (map read-string)))


(def day1-input (->> (string/split (slurp "src/main/notebook/day1.txt") #"\n")
                          (map read-string)))

(defn create-history
  [measurements]
  (reduce
    (fn [history current]
      (conj history (let [prev (first history)
                          status (cond
                                   (nil? prev) :first
                                   (> current (value prev)) :increased
                                   (< current (value prev)) :decreased
                                   :else :unchanged)]
                      (entry current status))))
    ()
    measurements))

(comment
  "compute answer test input"
  (->> day1-test-input
      create-history
      (filter #(= :increased (status %)))
      count))

(comment
  "compute answer part 1"
  (->> day1-input
       create-history
       (filter #(= :increased (status %)))
       count))

(defn total [frame]
  (apply + frame))

(comment
  "answer part 2"
  (let [frames (partition 3 1 day1-input)
        frame-pairs (partition 2 1 frames)]
    (count
      (filter
        (fn [[frame1 frame2]]
          (> (total frame2) (total frame1)))))))
