(ns notebook.day5
  (:require [clojure.string :as string]))


(defn load-input [filename]
  (let [lines (string/split
                (slurp filename)
                #"\n")
        vent-lines (map
                 (fn [line]
                   (let [[from to] (map
                                     (fn [string-coord]
                                       (mapv read-string (re-seq #"\d+" string-coord)))
                                     (string/split line #" -> "))]
                     [from to]))
                 lines)]
    vent-lines))

(defn abs [x]
  (Math/abs x))

(def test-input (load-input "src/main/notebook/day5.test.input"))

(def input (load-input "src/main/notebook/day5.input"))

(defn horizontal? [vent-line]
  (let [[from to] vent-line]
    (= (first from) (first to))))

(defn vertical? [vent-line]
  (let [[from to] vent-line]
    (= (second from) (second to))))

(defn sign [x]
  (if (pos? x) 1 -1))


;; FIXME: change the way it is computed for diagonal lines
(defn coords
  [vent-line]
  (let [[start end] vent-line
        [xstart ystart] start
        [xend yend] end
        dx (- xend xstart)
        dy (- yend ystart)]
    (for [x (range (inc (abs dx)))
          y (range (inc (abs dy)))]
      [(+ xstart (* (sign dx) x))
       (+ ystart (* (sign dy) y))])))

(do

  (defn filter-vent-lines
    [input]
    (filter
      #(or (horizontal? %)
           (vertical? %))
      input))

  (defn compute-world-map
    [vent-lines]
    (let [world-map {}]
      (reduce
        (fn [world-map vent-line]
          (let [line-coords (coords vent-line)]
            (reduce
              (fn [world-map coord]
                (update world-map coord #(if % (inc %) 1)))
              world-map
              line-coords)))
        world-map
        vent-lines)))

  (defn count-overlaps
    [world-map]
    (count
      (filter
        #(> (second %) 1)
        world-map)))

  (count-overlaps (compute-world-map test-input)))
